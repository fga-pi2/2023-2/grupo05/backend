#!/bin/bash
# docker-entrypoint.sh

# Function to wait for the database to be ready
wait_for_db() {
  echo "Waiting for database to become ready..."
  while ! pg_isready -h db -U postgres; do
    sleep 5
  done
}

# Wait for the database
wait_for_db

# Run migrations
npx typeorm-ts-node-commonjs migration:run -d ./src/drivers/db/data-source.ts

# Start your application
exec npm start
