FROM node:hydrogen-alpine

ENV TZ=America/Sao_Paulo

WORKDIR /usr/src/app

RUN apk add --no-cache postgresql-client

COPY package*.json ./

RUN npm install

COPY . .
RUN chmod +x ./docker-entrypoint.sh

EXPOSE 3000

CMD sh docker-entrypoint.sh
