## Backend - MedHelp

## Como rodar?

- Levante a base de dados dev `sudo docker-compose up -d db`
- Execute migrações `npx typeorm-ts-node-commonjs migration:run -d ./src/infrastructure/db/data-source.ts`
- Instale dependencias: `npm i`
- Rode: `npm run dev`

App se encontra em [http://localhost:3000](http://localhost:3000)

## Como fazer deploy?


**IP:** 15.229.75.24
**PORTA:** 80

Primeiramente pegue o `medhelp.pem` para poder acessar o servidor. (fala com renato)

Bote o `medhelp.pem` na sua pasta `~/.ssh/` 

Para copiar o projeto local pra máquina, basta rodar `make install` 

Para acessar a máquina, basta rodar `make ssh`

Acesse a pasta onde vc instalou (na pasta `deploy`, está configurado na variável `SERVER_PROJECT_PATH` do Makefile)

Levantar: `sudo docker-compose up --build -d`

### Debuggar Deploy

Processos na porta 80: `sudo lsof -i tcp:80`
Matar processo na porta 80: `sudo fuser -k 80/tcp`
Todas as conexoes de rede: `sudo netstat -nlp`
Mata todos os containers : `sudo netstat -nlp`
Conserta a versão do docker (se tiver errada): `sudo snap refresh --revision=2893 docker` 


#### !!!!! ATENÇÃO !!!!! CONSERTAR COISAS

Não REMOVA A BASE DE DADOS!!
Derrubar antigos: `sudo docker stop api db`
Derrubar antigos: `sudo docker rm api db`

## Calls mapeadas

GET /schedule/new
DELETE /schedule/list/:caregiverId
DELETE /schedule/:scheduleId
PUT /schedule/:scheduleId
GET /event/list/:caregiverId
POST /login
POST /longpoll

### Calls apenas para testes

POST /longpoll-test



## Estrutura de Arquivos

- backend.tf - configuração para provisionar máquina EC2 na AWS
- provider.tf - configuração para provisionar máquina EC2 na AWS
- main.tf - configuração para provisionar máquina EC2 na AWS
- Makefile - atalhos
- script - instalar terraform
- sonar-project.properties - configuração do sonar cloud