IP = 15.229.75.24
USER = ubuntu
PRJ_NAME = mock

SERVER_PROJECT_PATH = /home/$(USER)/deploy
# ALTERE A PASTA PARA O SEU PATH!
PATH_MY_OS = /home/renato/Desktop/pi2/backend

show:
	terraform show > resources.txt

run_system:
	echo "ALTERA NO ARQUIVO data-source.ts o field host = localhost para rodar em dev!!!"
	
	sudo docker-compose up -d db
	npx typeorm-ts-node-commonjs migration:run -d ./src/infrastructure/db/data-source.ts
	npm i
	npm run dev

call_POST_long_poll:
	curl -d '{"test": 123}' -H "Content-Type: application/json" -X POST http://localhost:3000/longpoll

call_POST_long_poll_test:
	curl -d '{"fileira": 2, "slot": 3}' -H "Content-Type: application/json" -X POST http://localhost:3000/longpoll-test

call_GET_schedule_new:
	curl http://localhost:3000/schedule/new

call_DELETE_schedule_list:
	curl -d '{"test": 123}' -H "Content-Type: application/json" -X DELETE http://localhost:3000/schedule/list/1
	# caregiverId

call_DELETE_schedule:
	curl -d '{"test": 123}' -H "Content-Type: application/json" -X DELETE http://localhost:3000/schedule/1
	# scheduleId

call_PUT_schedule:
	curl -d '{"test": 123}' -H "Content-Type: application/json" -X PUT http://localhost:3000/schedule/1
	# scheduleId

call_GET_event_list:
	curl http://localhost:3000/event/list/1
	# caregiverId

call_POST_login:
	curl -d '{"test": 123}' -H "Content-Type: application/json" -X POST http://localhost:3000/login

ssh:
	ssh -i ~/.ssh/medhelp.pem $(USER)@$(IP) 

install:
	rsync -av --exclude=".git" --exclude="node_modules" --exclude="pgdata" -e 'ssh -i ~/.ssh/medhelp.pem' . $(USER)@$(IP):$(SERVER_PROJECT_PATH)
	@echo "app installed on target:$(SERVER_PROJECT_PATH)"

donwload_file:
	scp -i ~/.ssh/medhelp.pem $(USER)@$(IP):$(SERVER_PROJECT_PATH)/$(FILE) $(PATH_MY_OS)/$(FILE)



call_PROD_POST_long_poll:
	curl -d '{"test": 123}' -H "Content-Type: application/json" -X POST http://15.229.75.24/longpoll

call_PROD_POST_long_poll_test:
	curl -d '{"fileira": 2, "slot": 3}' -H "Content-Type: application/json" -X POST http://15.229.75.24/longpoll-test

call_PROD_GET_schedule_new:
	curl http://15.229.75.24/schedule/new

call_PROD_DELETE_schedule_list:
	curl -d '{"test": 123}' -H "Content-Type: application/json" -X DELETE http://15.229.75.24/schedule/list/1
	# caregiverId

call_PROD_DELETE_schedule:
	curl -d '{"test": 123}' -H "Content-Type: application/json" -X DELETE http://15.229.75.24/schedule/1
	# scheduleId

call_PROD_PUT_schedule:
	curl -d '{"test": 123}' -H "Content-Type: application/json" -X PUT http://15.229.75.24/schedule/1
	# scheduleId

call_PROD_GET_event_list:
	curl http://15.229.75.24/event/list/1
	# caregiverId

call_PROD_POST_login:
	curl -d '{"test": 123}' -H "Content-Type: application/json" -X POST http://15.229.75.24/login
