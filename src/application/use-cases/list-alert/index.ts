import type AlertDomain from "../../../domain/alert";


export default class ListAlertsUseCase {
  constructor(private readonly alertsDomain: AlertDomain) {
    this.alertsDomain = alertsDomain;
  }
  async execute(caregiverId: number) {
    const alerts = await this.alertsDomain.list(caregiverId);
    return alerts;
  }
}