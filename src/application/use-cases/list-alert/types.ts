import { AlertState } from "../../../domain/alert/types";

export interface ListAlertsUseCaseReturn {
  results: AlertState[]
}