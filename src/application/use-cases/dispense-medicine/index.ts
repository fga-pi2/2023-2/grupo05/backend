import AlertDomain from "../../../domain/alert";
import { EmbeddingDomain } from "../../../domain/embedding";
import { MedicineDispenseArgs } from "../../../domain/embedding/types";
import EventDomain from "../../../domain/event";
import { EventState, EventStatusSlot } from "../../../domain/event/types";
import { EmbeddingSlotFormat } from "./types";

export default class DispenseMedicineUseCase {
  constructor(private eventDomain: EventDomain, private alertDomain: AlertDomain) { }

  async execute(): Promise<void> {
    const currentDate = new Date();
    currentDate.setSeconds(0, 0);

    const event: EventState = await this.eventDomain.findNextEventAfterDate(currentDate);
    const canDispenseMedicine: boolean = event && !event.isDispensed && event.idSlot && this.isEventTakingPlaceNow(event.takingDateTime);

    if (canDispenseMedicine) {
      const embeddingSlotFormat: MedicineDispenseArgs = this.convertToEmbeddingFormat(event.idSlot)
      EmbeddingDomain.addMedicineToDispense(embeddingSlotFormat);
      await this.eventDomain.update(event.eventId, { isDispensed: true, idSlot: null, statusSlot: EventStatusSlot.EMPTY })

      const caregiverId: number = event.caregiverId;
      const message: string = `O medicamento ${event.medicationName} foi liberado!`;
      const eventId: number = event.eventId;
      await this.alertDomain.create({
        caregiverId,
        eventId,
        message
      })
    }

  }
  private convertToEmbeddingFormat(idSlot: number): EmbeddingSlotFormat {
    return {
      "slot": idSlot % 100,
      "fileira": Math.floor(idSlot / 100)
    }
  }

  private isEventTakingPlaceNow(eventDateTime: Date): boolean {
    const currentDate = new Date();

    return (
      eventDateTime.getFullYear() === currentDate.getFullYear() &&
      eventDateTime.getMonth() === currentDate.getMonth() &&
      eventDateTime.getDate() === currentDate.getDate() &&
      eventDateTime.getHours() === currentDate.getHours() &&
      eventDateTime.getMinutes() === currentDate.getMinutes()
    );

  }
}