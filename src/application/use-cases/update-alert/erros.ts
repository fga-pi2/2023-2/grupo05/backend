export class AlertError extends Error {
    constructor(message) {
        super(message);
        this.name = 'AlertError';
        Object.setPrototypeOf(this, AlertError.prototype);
    }
}
export class AlertNotFoundError extends AlertError {
    constructor() {
        super("Alert not found.");
        Object.setPrototypeOf(this, AlertNotFoundError.prototype);
    }
}
export class InvalidAlertIdError extends AlertError {
    constructor() {
        super("Invalid alert id.");
        Object.setPrototypeOf(this, InvalidAlertIdError.prototype);
    }
}
export class InvalidAlertMessageError extends AlertError {
    constructor() {
        super("Invalid alert message.");
        Object.setPrototypeOf(this, InvalidAlertMessageError.prototype);
    }
}
export class InvalidAlertSeenError extends AlertError {
    constructor() {
        super("Invalid alert seen.");
        Object.setPrototypeOf(this, InvalidAlertSeenError.prototype);
    }
}
export class InvalidAlertCaregiverIdError extends AlertError {
    constructor() {
        super("Invalid alert caregiver id.");
        Object.setPrototypeOf(this, InvalidAlertCaregiverIdError.prototype);
    }
}