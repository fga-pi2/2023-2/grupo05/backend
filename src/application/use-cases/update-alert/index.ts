import { UpdateAlertUseCaseArgs } from "./input";
import AlertDomain from "../../../domain/alert";
import { AlertNotFoundError } from "./erros";
import { AlertState } from "../../../domain/alert/types";

export default class UpdateAlertUseCase {
    constructor(private alertDomain: AlertDomain) { }

    async execute(args: UpdateAlertUseCaseArgs): Promise<AlertState> {
        const alert = await this.alertDomain.findById(args["alertId"]);
        Object.keys(args).forEach(key => args[key] === undefined && delete args[key]);

        if (!alert) throw new AlertNotFoundError();
        const returned_value = await this.alertDomain.update({ ...args });
        return returned_value;
    }

}