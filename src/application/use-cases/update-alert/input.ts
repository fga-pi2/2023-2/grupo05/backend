export interface UpdateAlertUseCaseArgs {
    alertId: number;
    message?: string;
    seen?: boolean;
    caregiverId?: number;
    eventId?: number;
}