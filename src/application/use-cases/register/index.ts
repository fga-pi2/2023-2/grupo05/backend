import UserDomain from "../../../domain/user";
import { UserState } from "../../../domain/user/types";
import { RegisterUseCaseArgs } from "./input";
import * as jwt from "jsonwebtoken";

export default class RegisterUseCase {
  constructor(private userDomain: UserDomain) { }

  async execute({
    caregiverName,
    takerName,
    phoneNumber,
    email,
    password,
  }: RegisterUseCaseArgs) {
    let user: UserState = await this.userDomain.create({
      caregiverName,
      takerName,
      phoneNumber,
      email,
      password,
    });

    const token = jwt.sign({ email: email }, "JWT_SECRET", {
      expiresIn: Number(604800), //604800 = uma semana em segundos
    });

    delete user.passwordHash; // remove passwordHash from user 

    return {
      user,
      token,
    };
  }
}
