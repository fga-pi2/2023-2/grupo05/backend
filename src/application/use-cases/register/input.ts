export type RegisterUseCaseArgs = {
  caregiverName: string;
  takerName: string;
  phoneNumber: string;
  email: string;
  password: string;
};
