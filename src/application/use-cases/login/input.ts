export type LoginUseCaseArgs = {
  email: string;
  password: string;
};
