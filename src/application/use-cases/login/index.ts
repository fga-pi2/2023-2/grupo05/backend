import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
import { LoginUseCaseArgs } from "./input";
import { UnauthorizedError } from "./errors";
import { UserState } from "../../../domain/user/types";
import UserDomain from "../../../domain/user";

export default class LoginUseCase {
  constructor(private userDomain: UserDomain) { }

  async execute({ email, password }: LoginUseCaseArgs) {
    let user: UserState = await this.userDomain.findByEmail({ email });

    if (!user) throw new UnauthorizedError();

    const isValidPassword = bcrypt.compareSync(password, user.passwordHash);

    if (!isValidPassword) throw new UnauthorizedError();

    const token = jwt.sign({ userId: user.caregiverId }, "JWT_SECRET", {
      expiresIn: 604800, // 604800 seconds = one week
    });

    const { passwordHash, ...userData } = user;

    return {
      user: userData,
      token,
    };
  }
}
