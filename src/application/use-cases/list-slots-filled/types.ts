

export type ListSlotsFilledReturn = {
  fileira: number;
  slot: number;
  medicationName: string;
  medicationDescription: string;
}
