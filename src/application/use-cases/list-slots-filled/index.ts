import EventDomain from "../../../domain/event";
import { EventState } from "../../../domain/event/types";
import ScheduleDomain from "../../../domain/schedule";
import { ListSlotsFilledArgs } from "./input";
import { ListSlotsFilledReturn } from "./types";


export default class ListSlotsFilledUseCase {
  constructor(private eventDomain: EventDomain, private scheduleDomain: ScheduleDomain) { };

  async execute({ caregiverId }: ListSlotsFilledArgs): Promise<ListSlotsFilledReturn[]> {
    const schedules = await this.scheduleDomain.findByCaregiver({ caregiverId });
    const scheduleIds = schedules.map(schedule => schedule.scheduleId);
    const slotsFilled = await this.eventDomain.findPlaced({ scheduleIds });
    return slotsFilled.map(event => this.convertSlotsFormat(event));
  }

  private convertSlotsFormat(event: EventState): ListSlotsFilledReturn {
    return {
      "slot": event.idSlot % 100,
      "fileira": Math.floor(event.idSlot / 100),
      "medicationName": event.medicationName,
      "medicationDescription": event.medicationDescription
    }
  }
}