
export type DispenseDrawerUseCaseArgs = {
  open: boolean;
};