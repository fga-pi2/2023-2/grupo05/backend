import AlertDomain from "../../../domain/alert";
import EventDomain from "../../../domain/event";
import { EventState, EventStatus } from "../../../domain/event/types";
import { DispenseDrawerUseCaseArgs } from "./input";

export default class DispenseDrawerUseCase {
  constructor(private eventDomain: EventDomain, private alertDomain: AlertDomain) { }

  async execute(args: DispenseDrawerUseCaseArgs): Promise<void> {
    if (!args.open) {
      return;
    }

    const currentDate = new Date();
    const events = await this.getDispensedEvents(currentDate);

    if (events.length > 0) {
      await this.markEventsAsTaken(events);
      return;
    }

    await this.createDrawerOpenedAlert();
  }

  private async getDispensedEvents(currentDate: Date): Promise<EventState[]> {
    return await this.eventDomain.getNotTakenDispensed(currentDate);
  }

  private async markEventsAsTaken(events: EventState[]): Promise<void> {
    await Promise.all(events.map(event => this.markEventAsTaken(event)));
  }

  private async markEventAsTaken(event: EventState): Promise<void> {
    await this.eventDomain.update(event.eventId, { status: EventStatus.TAKEN });
    await this.createMedicationTakenAlert(event);
  }

  private async createMedicationTakenAlert(event: EventState): Promise<void> {
    const message = `O medicamento ${event.medicationName} foi tomado!`;
    await this.alertDomain.create({
      caregiverId: event.caregiverId,
      eventId: event.eventId,
      message
    });
  }

  private async createDrawerOpenedAlert(): Promise<void> {
    const message = `A gaveta foi aberta!`;
    await this.alertDomain.create({
      caregiverId: 1, // Assuming caregiver ID is static 
      message
    });
  }
}
