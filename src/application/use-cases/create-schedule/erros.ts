/**
 * Custom errors for Schedule domain
 */

export class ScheduleError extends Error {
  constructor(message: string) {
    super(message);
    this.name = 'ScheduleError';
    Object.setPrototypeOf(this, ScheduleError.prototype);
  }
}


export class EndDateError extends ScheduleError {
  constructor() {
    super("The end date can't be earlier than the start date.");
    Object.setPrototypeOf(this, EndDateError.prototype);
  }
}

export class IntervalError extends ScheduleError {
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, IntervalError.prototype);
  }
}

export class ZeroIntervalError extends IntervalError {
  constructor() {
    super("The hour and minutes can't both be zero.");
    Object.setPrototypeOf(this, ZeroIntervalError.prototype);
  }
}

export class NegativeIntervalError extends IntervalError {
  constructor() {
    super("The hour or minutes can't be negative.");
    Object.setPrototypeOf(this, NegativeIntervalError.prototype);
  }
}
export class InvalidTimeFormatError extends Error {
  constructor(receivedFormat: string) {
    super(`Invalid time format received: '${receivedFormat}'. Expected format: 'HH:MM'.`);
    this.name = 'InvalidTimeFormatError';
    Object.setPrototypeOf(this, InvalidTimeFormatError.prototype);
  }
}

export class SlotsNotFreeError extends ScheduleError {
  constructor(unplacedSlots: number[]) {
    const slotList = unplacedSlots.length > 1
      ? `${unplacedSlots.slice(0, -1).join(", ")} and ${unplacedSlots.slice(-1)}`
      : unplacedSlots.join();
    super(`The slots ${slotList} are not free.`);
    this.name = 'SlotsNotFreeError';
    Object.setPrototypeOf(this, SlotsNotFreeError.prototype);
  }
}
