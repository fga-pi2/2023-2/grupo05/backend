import { CreateScheduleUseCaseArgs, Interval } from "./input";
import ScheduleDomain from "../../../domain/schedule";
import EventDomain from "../../../domain/event";
import { EndDateError, NegativeIntervalError, ZeroIntervalError, InvalidTimeFormatError, SlotsNotFreeError } from "./erros";
import { EventStatusSlot } from "../../../domain/event/types";
import { DateTime } from 'luxon';

export default class CreateScheduleUseCase {
  constructor(private scheduleDomain: ScheduleDomain, private eventDomain: EventDomain) { }

  async execute(args: CreateScheduleUseCaseArgs): Promise<void> {
    this.validateArgs(args);
    await this.checkSlotsAvailability(args);

    const startDate = DateTime.fromISO(args.startDate);
    const endDate = DateTime.fromISO(args.endDate).set({ hour: 23, minute: 59, second: 59, millisecond: 999 });

    const { slots, ...scheduleArgs } = args;
    const schedule = await this.scheduleDomain.create({
      ...scheduleArgs,
      startDate: startDate.toJSDate(),
      endDate: endDate.toJSDate()
    });

    const scheduleId = schedule.scheduleId;

    if (args.scheduleInterval) {
      await this.createEventsWithInterval(args, scheduleId, startDate, endDate);
    } else {
      await this.createEventsWithSpecificTimes(args, scheduleId);
    }
  }

  private validateArgs(args: CreateScheduleUseCaseArgs): void {
    this.validateDates(args.startDate, args.endDate);
    this.validateIntervalHours(args.intervalHours);
    if (args.startTime) this.validateTimeFormat(args.startTime);
    if (args.times) this.validateTimesArray(args.times);
  }

  private validateTimeFormat(startTime: string): void {
    if (!this.isValidTimeFormat(startTime)) {
      throw new InvalidTimeFormatError(startTime);
    }
  }

  private validateTimesArray(times: string[]): void {
    times.forEach(time => {
      if (!this.isValidTimeFormat(time)) {
        throw new InvalidTimeFormatError(time);
      }
    });
  }

  private validateDates(startDate: string, endDate: string): void {
    const startDateTime = DateTime.fromISO(startDate);
    const endDateTime = DateTime.fromISO(endDate).set({ hour: 23, minute: 59, second: 59, millisecond: 999 });

    if (endDateTime < startDateTime) {
      throw new EndDateError();
    }
  }

  private validateIntervalHours(intervalHours: any): void {
    if (intervalHours && (intervalHours.hours < 0 || intervalHours.minutes < 0)) {
      throw new NegativeIntervalError();
    }
    if (intervalHours && intervalHours.hours === 0 && intervalHours.minutes === 0) {
      throw new ZeroIntervalError();
    }
  }

  private async checkSlotsAvailability(args: CreateScheduleUseCaseArgs): Promise<void> {
    const schedules = await this.scheduleDomain.findByCaregiver({ caregiverId: args.caregiverId });
    const scheduleIds = schedules.map(schedule => schedule.scheduleId);

    const slotsPlaced = await this.eventDomain.findPlaced({ scheduleIds });
    const placedSlotIds = slotsPlaced.map(event => event.idSlot);
    const placedProvidedSlots = args.slots.filter(slot => placedSlotIds.includes(slot));

    if (placedProvidedSlots.length > 0) {
      throw new SlotsNotFreeError(placedProvidedSlots);
    }
  }

  private async createEventsWithInterval(args: CreateScheduleUseCaseArgs, scheduleId: number, startDate: DateTime, endDate: DateTime): Promise<void> {
    let slotIndex = 0;
    const [startHours, startMinutes] = args.startTime.split(':').map(Number);
    let currentTime = startDate.set({ hour: startHours, minute: startMinutes, second: 0, millisecond: 0 });

    while (currentTime <= endDate) {
      if (currentTime > DateTime.now()) {
        await this.createEvent(args, scheduleId, currentTime, slotIndex);
        slotIndex++;
      }
      currentTime = this.incrementTime(currentTime, args.intervalHours);
    }
  }

  private incrementTime(currentTime: DateTime, interval: Interval): DateTime {
    return currentTime.plus({ hours: interval.hours, minutes: interval.minutes });
  }

  private async createEventsWithSpecificTimes(args: CreateScheduleUseCaseArgs, scheduleId: number): Promise<void> {
    args.times = this.sortTimes(args.times);

    let slotIndex = 0;
    const startDate = DateTime.fromISO(args.startDate);
    const endDate = DateTime.fromISO(args.endDate).set({ hour: 23, minute: 59, second: 59, millisecond: 999 });

    for (let date = startDate; date <= endDate; date = date.plus({ days: 1 })) {
      for (const time of args.times) {
        let takingDateTime = date.set({ hour: parseInt(time.split(':')[0]), minute: parseInt(time.split(':')[1]), second: 0, millisecond: 0 });
        if (takingDateTime > DateTime.now()) {
          await this.createEvent(args, scheduleId, takingDateTime, slotIndex);
          slotIndex++;
        }
      }
    }
  }

  private async createEvent(args: CreateScheduleUseCaseArgs, scheduleId: number, dateTime: DateTime, slotIndex: number): Promise<void> {
    const idSlot = slotIndex < args.slots.length ? args.slots[slotIndex] : null;
    const statusSlot = idSlot ? EventStatusSlot.PLACED : EventStatusSlot.EMPTY;

    await this.eventDomain.create({
      scheduleId,
      statusSlot,
      takingDateTime: dateTime.toJSDate(),
      idSlot
    });
  }

  private isValidTimeFormat(time: string): boolean {
    return /^([01]?\d|2[0-3]):[0-5]\d(:[0-5]\d)?$/.test(time);
  }

  private sortTimes(times: string[]): string[] {
    return times.sort((a, b) => {
      const [hoursA, minutesA] = a.split(':').map(Number);
      const [hoursB, minutesB] = b.split(':').map(Number);

      return hoursA * 60 + minutesA - (hoursB * 60 + minutesB);
    });
  }
}
