// 
// Assuming necessary imports
import { EventState } from '../../../../domain/event/types';
import CreateScheduleUseCase from '..';

import { EndDateError, NegativeIntervalError, ZeroIntervalError, InvalidTimeFormatError, SlotsNotFreeError } from '../erros';
import { ScheduleState } from '../../../../domain/schedule/types';
import ScheduleDomain from '../../../../domain/schedule';
import ScheduleRepository from '../../../../domain/schedule/repository';
import EventRepository from '../../../../domain/event/repository';
import EventDomain from '../../../../domain/event';

const mockScheduleRepository: jest.Mocked<ScheduleRepository> = {
  create: jest.fn(),
  findByCaregiver: jest.fn(),
  findByMedicationName: jest.fn(),
  delete: jest.fn(),
  findOneBy: jest.fn(),
  update: jest.fn()


};

const mockEventRepository: jest.Mocked<EventRepository> = {
  create: jest.fn(),
  findPlaced: jest.fn(),
  findByRange: jest.fn(),
  update: jest.fn(),
  delete: jest.fn(),
  findNextEventAfterDate: jest.fn(),
  getNotTakenDispensed: jest.fn()
};


const mockScheduleDomain = new ScheduleDomain(mockScheduleRepository)
const mockEventDomain = new EventDomain(mockEventRepository)


const useCase = new CreateScheduleUseCase(
  mockScheduleDomain,
  mockEventDomain
)
describe('CreateScheduleUseCase', () => {
  const baseArgs = {
    scheduleInterval: true,
    endDate: '2024-01-01',
    startDate: '2023-12-01',
    startTime: '08:00',
    intervalHours: { hours: 2, minutes: 30 },
    medicationName: 'Medication',
    medicationDescription: "Medication description",
    slots: [1, 2, 3],
    caregiverId: 123,
    times: undefined
  };

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should create a schedule with interval successfully', async () => {
    // Arrange
    mockScheduleRepository.findByCaregiver.mockResolvedValue([])
    mockEventRepository.findPlaced.mockResolvedValue([])
    mockEventRepository.create.mockResolvedValue()
    mockScheduleRepository.create.mockResolvedValue({
      scheduleId: 1,
      scheduleInterval: true,
      endDate: new Date('2024-01-01'),
      startDate: new Date('2023-12-01'),
      startTime: '08:00',
      intervalHours: { hours: 2, minutes: 30 },
      medicationName: 'Medication',
      medicationDescription: "Medication description",
      slots: [1, 2, 3],
      caregiverId: 123,
      times: undefined,
      createdAt: new Date(),
      updatedAt: new Date()
    } as ScheduleState)

    await useCase.execute(baseArgs);

    expect(mockScheduleRepository.findByCaregiver).toHaveBeenCalledWith({ caregiverId: 123 })
    expect(mockEventRepository.findPlaced).toHaveBeenCalledWith({ scheduleIds: [] })
    expect(mockScheduleRepository.create).toHaveBeenCalledWith({
      scheduleInterval: true,
      endDate: new Date('2024-01-01'),
      startDate: new Date('2023-12-01'),
      startTime: '08:00',
      intervalHours: { hours: 2, minutes: 30 },
      times: undefined,
      medicationName: 'Medication',
      medicationDescription: "Medication description",
      caregiverId: 123,
    });

  });

  it('should create a schedule with times successfully', async () => {
    // Arrange
    mockScheduleRepository.findByCaregiver.mockResolvedValue([])
    mockEventRepository.findPlaced.mockResolvedValue([])
    mockEventRepository.create.mockResolvedValue()
    mockScheduleRepository.create.mockResolvedValue({
      scheduleId: 1,
      scheduleInterval: false,
      endDate: new Date('2024-01-01'),
      startDate: new Date('2023-12-01'),
      startTime: undefined,
      intervalHours: undefined,
      medicationName: 'Medication',
      medicationDescription: "Medication description",
      slots: [1, 2, 3],
      caregiverId: 123,
      times: ["8:40", "8:30", "8:10", "12:00"],
      createdAt: new Date(),
      updatedAt: new Date()
    } as ScheduleState)
    const args = { ...baseArgs, scheduleInterval: false, intervalHours: undefined, startTime: undefined, times: ["8:40", "8:30", "8:10", "12:00"] };
    await useCase.execute(args);

    expect(mockScheduleRepository.findByCaregiver).toHaveBeenCalledWith({ caregiverId: 123 })
    expect(mockEventRepository.findPlaced).toHaveBeenCalledWith({ scheduleIds: [] })
    expect(mockScheduleRepository.create).toHaveBeenCalledWith({
      scheduleInterval: false,
      endDate: new Date('2024-01-01'),
      startDate: new Date('2023-12-01'),
      startTime: undefined,
      intervalHours: undefined,
      times: ["8:10", "8:30", "8:40", "12:00"],
      medicationName: 'Medication',
      medicationDescription: "Medication description",
      caregiverId: 123,
    });

  });

  // Test for EndDateError
  it('should throw EndDateError for invalid end date', async () => {
    const args = { ...baseArgs, endDate: '2023-11-30' };
    await expect(useCase.execute(args)).rejects.toThrow(EndDateError);
  });

  // Test for ZeroIntervalError
  it('should throw ZeroIntervalError for zero interval', async () => {
    const args = { ...baseArgs, intervalHours: { hours: 0, minutes: 0 } };
    await expect(useCase.execute(args)).rejects.toThrow(ZeroIntervalError);
  });

  // Test for NegativeIntervalError
  it('should throw NegativeIntervalError for negative interval', async () => {
    const args = { ...baseArgs, intervalHours: { hours: -1, minutes: 0 } };
    await expect(useCase.execute(args)).rejects.toThrow(NegativeIntervalError);
  });

  // Test for InvalidTimeFormatError
  it('should throw InvalidTimeFormatError for incorrect time format in startTime args', async () => {
    const args = { ...baseArgs, startTime: '25:00' }; // Invalid time format
    await expect(useCase.execute(args)).rejects.toThrow(InvalidTimeFormatError);
  });
  it('should throw InvalidTimeFormatError for incorrect time format in times args', async () => {
    const args = { ...baseArgs, scheduleInterval: false, intervalHours: undefined, startTime: undefined, times: ["8:40", "8:30", "25:00"] };
    await expect(useCase.execute(args)).rejects.toThrow(InvalidTimeFormatError);
  });

  // // Test for SlotsNotFreeError
  it('should throw SlotsNotFreeError if provided slots are not free', async () => {
    mockEventRepository.findPlaced.mockResolvedValue([
      { idSlot: 1 } as EventState // This simulates that slot 1 is already taken
    ]);
    const args = { ...baseArgs, slots: [1] }; // Slot 1 is not free
    await expect(useCase.execute(args)).rejects.toThrow(SlotsNotFreeError);
  });

});
