export type CreateScheduleUseCaseArgs = {
  scheduleInterval: boolean;
  endDate: string | null;
  startDate: string | null;
  startTime: string | null;
  intervalHours: Interval | null;
  times: string[] | null;
  medicationName: string;
  medicationDescription: string | null;
  slots: number[];
  caregiverId: number; // Uncomment and use if the CaregiverTaker entity is included
}

export type Interval = { hours: number, minutes: number } 