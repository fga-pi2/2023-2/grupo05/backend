import AlertDomain from "../../../domain/alert";
import { EmbeddingDomain } from "../../../domain/embedding";
import EventDomain from "../../../domain/event";
import { EventState } from "../../../domain/event/types";

export default class DispenseTrashUseCase {
  constructor(private eventDomain: EventDomain, private alertDomain: AlertDomain) { }

  async execute(): Promise<void> {
    const currentDate = this.getCurrentDateWithZeroedSeconds();
    const events = await this.getDiscardableEvents(currentDate);

    if (events.length > 0) {
      EmbeddingDomain.openTrash();
      await this.discardEvents(events);
    }
  }

  private getCurrentDateWithZeroedSeconds(): Date {
    const currentDate = new Date();
    currentDate.setSeconds(0, 0);
    return currentDate;
  }

  private async getDiscardableEvents(currentDate: Date): Promise<EventState[]> {
    const threeMinutesAgo = this.getThreeMinutesAgoDate();
    const events = await this.eventDomain.getNotTakenDispensed(currentDate);
    return events.filter(event => this.isEventDiscardable(event, threeMinutesAgo));
  }

  private getThreeMinutesAgoDate(): Date {
    const date = new Date();
    date.setMinutes(date.getMinutes() - 3);
    return date;
  }

  private isEventDiscardable(event: EventState, thresholdDate: Date): boolean {
    return !event.isDiscarded && new Date(event.takingDateTime) < thresholdDate;
  }

  private async discardEvents(events: EventState[]): Promise<void> {
    for (const event of events) {
      await this.updateEventToDiscarded(event);
    }
  }

  private async updateEventToDiscarded(event: EventState): Promise<void> {
    await this.eventDomain.update(event.eventId, { isDiscarded: true });
    await this.createDiscardAlert(event);
  }

  private async createDiscardAlert(event: EventState): Promise<void> {
    const message = `O medicamento ${event.medicationName} foi descartado!`;
    await this.alertDomain.create({
      caregiverId: event.caregiverId,
      eventId: event.eventId,
      message
    });
  }
}
