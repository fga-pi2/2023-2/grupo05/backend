

import ListEventUseCase from '..';
import EventDomain from '../../../../domain/event';
import ScheduleDomain from '../../../../domain/schedule';
import { EventState, EventStatus, EventStatusSlot } from '../../../../domain/event/types';
import { ScheduleState } from '../../../../domain/schedule/types';

import ScheduleRepository from '../../../../domain/schedule/repository';
import EventRepository from '../../../../domain/event/repository';

const scheduleRepository: jest.Mocked<ScheduleRepository> = {
  create: jest.fn(),
  findByCaregiver: jest.fn(),
  findByMedicationName: jest.fn(),
  delete: jest.fn(),
  findOneBy: jest.fn(),
  update: jest.fn()

};

const eventRepository: jest.Mocked<EventRepository> = {
  create: jest.fn(),
  findPlaced: jest.fn(),
  findByRange: jest.fn(),
  update: jest.fn(),
  delete: jest.fn(),
  findNextEventAfterDate: jest.fn(),
  getNotTakenDispensed: jest.fn(),
};





const mockEvent: EventState = {
  caregiverId: 123,
  eventId: 1,
  scheduleId: 1,
  status: EventStatus.NOT_TAKEN,
  statusSlot: EventStatusSlot.EMPTY,
  takingDateTime: new Date('2023-12-01T08:00:00Z'),
  idSlot: 5,
  createdAt: new Date(),
  updatedAt: new Date(),
  isDispensed: false,
  medicationDescription: "medication description",
  medicationName: "medication name",
  isDiscarded: false
};


const mockSchedule: ScheduleState = {
  scheduleInterval: true,
  scheduleId: 1,
  endDate: new Date('2024-01-01'),
  startDate: new Date('2023-12-01'),
  startTime: '08:00',
  intervalHours: { hours: 2, minutes: 30 },
  medicationName: 'Medication',
  medicationDescription: "Medication description",
  caregiverId: 123,
  times: undefined,
  createdAt: new Date(),
  updatedAt: new Date()
}

describe('ListEventUseCase', () => {
  let eventDomain: EventDomain;
  let scheduleDomain: ScheduleDomain;
  let useCase: ListEventUseCase;

  beforeEach(() => {
    scheduleDomain = new ScheduleDomain(scheduleRepository)
    eventDomain = new EventDomain(eventRepository)
    useCase = new ListEventUseCase(eventDomain, scheduleDomain);

    scheduleRepository.findByCaregiver.mockResolvedValue([mockSchedule]);
    eventRepository.findByRange.mockResolvedValue([mockEvent]);
  });

  it('should fetch events by caregiverId within the specified date range', async () => {
    const caregiverId = 123;
    const targetDate = new Date('2023-12-01');

    const result = await useCase.execute({ caregiverId, targetDate });

    expect(scheduleRepository.findByCaregiver).toHaveBeenCalledWith({ caregiverId });
    expect(eventRepository.findByRange).toHaveBeenCalledWith({
      scheduleIds: [1],
      startDate: expect.any(Date),
      endDate: expect.any(Date)
    });

    expect(result).toEqual([{
      day: expect.any(Date),
      data: [mockEvent]
    }]);
  });

});
