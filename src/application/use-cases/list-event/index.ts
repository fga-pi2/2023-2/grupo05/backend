import EventDomain from "../../../domain/event";
import { ListEventUseCaseArgs } from "./input";
import { ListEventUseCaseReturn } from "./types";
import { ScheduleState } from "../../../domain/schedule/types";
import ScheduleDomain from "../../../domain/schedule";
import { EventState } from "../../../domain/event/types";

export default class ListEventUseCase {
  constructor(private eventDomain: EventDomain, private scheduleDomain: ScheduleDomain) { };

  async execute({ caregiverId, targetDate }: ListEventUseCaseArgs): Promise<ListEventUseCaseReturn[]> {
    const schedules: ScheduleState[] = await this.scheduleDomain.findByCaregiver({ caregiverId });
    const { startDate, endDate } = this.calculateDateRange(targetDate);
    const events = await this.eventDomain.findByRange({
      scheduleIds: schedules.map(schedule => schedule.scheduleId),
      startDate,
      endDate
    });

    const groupedEvents = this.groupAndSortEvents(events);
    return groupedEvents;
  }

  private calculateDateRange(targetDate: Date): { startDate: Date, endDate: Date } {
    const target = new Date(targetDate);

    const startDate = new Date(Date.UTC(target.getUTCFullYear(), target.getUTCMonth(), target.getUTCDate() - 1));
    const endDate = new Date(Date.UTC(target.getUTCFullYear(), target.getUTCMonth(), target.getUTCDate() + 1, 23, 59, 59, 999));

    return { startDate, endDate };
  }


  private groupAndSortEvents(events: EventState[]): ListEventUseCaseReturn[] {
    const groupedEvents: ListEventUseCaseReturn[] = [];
    events.forEach(event => {
      const eventDay = event.takingDateTime.toISOString().split('T')[0];
      let group = groupedEvents.find(g => g.day.toISOString().split('T')[0] === eventDay);
      if (!group) {
        group = { day: new Date(eventDay), data: [] };
        groupedEvents.push(group);
      }
      group.data.push(event);
    });

    groupedEvents.forEach(group => {
      group.data.sort((a, b) => a.takingDateTime.getTime() - b.takingDateTime.getTime());
    });

    return groupedEvents;
  }
}
