
export class FetchingEventsError extends Error {
  constructor(message: string = "Error fetching events.") {
    super(message);
    this.name = 'FetchingEventsError';
    Object.setPrototypeOf(this, FetchingEventsError.prototype);
  }
}
