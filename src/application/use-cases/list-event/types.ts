import { EventState } from "../../../domain/event/types";

export interface ListEventUseCaseReturn {
  day: Date,
  data: EventState[]
}