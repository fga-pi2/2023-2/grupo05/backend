export type ListEventUseCaseArgs = {
  caregiverId: number;
  targetDate: Date
}
