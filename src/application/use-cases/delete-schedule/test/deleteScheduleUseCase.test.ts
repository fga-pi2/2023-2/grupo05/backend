import DeleteScheduleUseCase from '..';
import EventDomain from '../../../../domain/event';
import EventRepository from '../../../../domain/event/repository';
import ScheduleDomain from '../../../../domain/schedule';
import ScheduleRepository from '../../../../domain/schedule/repository';
import { ScheduleState } from '../../../../domain/schedule/types';
import { InvalidScheduleError, ScheduleNotFoundError } from '../erros';

describe('DeleteScheduleUseCase', () => {
  let scheduleDomain: ScheduleDomain;
  let eventDomain: EventDomain;
  let deleteScheduleUseCase: DeleteScheduleUseCase;
  let mockScheduleRepository: jest.Mocked<ScheduleRepository>
  let mockEventRepository: jest.Mocked<EventRepository>
  beforeEach(() => {
    mockScheduleRepository = {
      create: jest.fn(),
      findByCaregiver: jest.fn(),
      findByMedicationName: jest.fn(),
      delete: jest.fn(),
      findOneBy: jest.fn(),
      update: jest.fn()
    };
    mockEventRepository = {
      create: jest.fn(),
      findPlaced: jest.fn(),
      findByRange: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
      findNextEventAfterDate: jest.fn(),
      getNotTakenDispensed: jest.fn()
    };
    scheduleDomain = new ScheduleDomain(mockScheduleRepository);
    eventDomain = new EventDomain(mockEventRepository);
    deleteScheduleUseCase = new DeleteScheduleUseCase(scheduleDomain, eventDomain);
  });
  it('successfully deletes a schedule', async () => {
    const scheduleId = 1;
    const now = new Date();

    const defaultMockedSchedule: ScheduleState = {
      scheduleInterval: false,
      scheduleId: 1, // Assign a mock ID
      startDate: new Date(now.getFullYear(), now.getMonth(), now.getDate()), // Mock start date as today
      endDate: new Date(now.getFullYear(), now.getMonth(), now.getDate() + 7), // Mock end date as one week from today
      startTime: undefined,
      intervalHours: undefined, // Mock interval of every 6 hours
      times: ['08:00', '14:00', '20:00'], // Mock times array
      medicationName: 'Paracetamol', // Mock medication name
      medicationDescription: 'Used to treat pain and fever', // Mock medication description
      createdAt: now, // Use the current time for creation
      updatedAt: now, // Use the current time for last update
      caregiverId: 123, // Mock caregiver ID
    }
    mockScheduleRepository.findOneBy.mockResolvedValue(defaultMockedSchedule);
    mockEventRepository.findByRange.mockResolvedValue([]);
    await deleteScheduleUseCase.execute(scheduleId);
    expect(mockScheduleRepository.findOneBy).toHaveBeenCalledWith({ scheduleId });
    expect(mockScheduleRepository.delete).toHaveBeenCalledWith(scheduleId);
  });

  it('throws ScheduleNotFoundError if the schedule does not exist', async () => {
    const scheduleId = 99;
    mockScheduleRepository.findOneBy.mockResolvedValue(null); // Mock not finding a schedule
    await expect(deleteScheduleUseCase.execute(scheduleId))
      .rejects.toThrow(ScheduleNotFoundError);
  });

  it('throws InvalidScheduleError for invalid scheduleId', async () => {
    await expect(deleteScheduleUseCase.execute(null))
      .rejects.toThrow(InvalidScheduleError);
    await expect(deleteScheduleUseCase.execute(undefined))
      .rejects.toThrow(InvalidScheduleError);
    await expect(deleteScheduleUseCase.execute('string' as any))
      .rejects.toThrow(InvalidScheduleError);
  });
});
