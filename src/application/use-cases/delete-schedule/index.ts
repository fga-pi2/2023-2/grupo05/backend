import EventDomain from "../../../domain/event";
import ScheduleDomain from "../../../domain/schedule";
import { InvalidScheduleError, ScheduleNotFoundError } from "./erros";


export default class DeleteScheduleUseCase {
  constructor(private scheduleDomain: ScheduleDomain, private eventDomain: EventDomain) { };

  async execute(scheduleIdInput: number | string): Promise<void> {
    const scheduleId = Number(scheduleIdInput);

    if (!scheduleId || isNaN(scheduleId)) {
      throw new InvalidScheduleError();
    }

    const schedule = await this.scheduleDomain.findOneBy({ scheduleId });
    if (!schedule) {
      throw new ScheduleNotFoundError(scheduleId);
    }


    const events = await this.eventDomain.findByRange({
      scheduleIds: [scheduleId],
      startDate: schedule.startDate,
      endDate: schedule.endDate
    });
    const now = new Date();

    const eventsToDelete = events.filter(event => event.takingDateTime > now);
    await Promise.all(eventsToDelete.map(event => this.eventDomain.delete(event.eventId)));

    await this.scheduleDomain.delete(scheduleId);
  }
}