export class InvalidScheduleError extends Error {
  constructor(message: string = "Invalid scheduleId provided.") {
    super(message);
    this.name = 'InvalidScheduleError';
    Object.setPrototypeOf(this, InvalidScheduleError.prototype);
  }
}



export class ScheduleNotFoundError extends Error {
  constructor(scheduleId: number) {
    super(`Schedule with id ${scheduleId} not found.`);
    this.name = 'ScheduleNotFoundError';
    Object.setPrototypeOf(this, ScheduleNotFoundError.prototype);
  }
}
