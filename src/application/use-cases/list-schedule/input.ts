export type ListScheduleUseCaseArgs = {
  caregiverId: number;
  medicationName?: string;
}
