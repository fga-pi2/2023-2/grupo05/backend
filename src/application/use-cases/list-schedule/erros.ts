
export class FetchingSchedulesError extends Error {
  constructor(message: string = "Error fetching schedules.") {
    super(message);
    this.name = 'FetchingSchedulesError';
    Object.setPrototypeOf(this, FetchingSchedulesError.prototype);
  }
}
