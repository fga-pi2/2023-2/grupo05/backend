import ScheduleDomain from "../../../domain/schedule";
import { ListScheduleUseCaseArgs } from "./input";
import { ListScheduleUseCaseReturn } from "./types";
import { ScheduleState } from "../../../domain/schedule/types";
import { FetchingSchedulesError } from "./erros";
import EventDomain from "../../../domain/event";
import { DateTime } from 'luxon';

export default class ListScheduleUseCase {
  constructor(private scheduleDomain: ScheduleDomain, private eventDomain: EventDomain) { };

  async execute({ caregiverId, medicationName }: ListScheduleUseCaseArgs): Promise<ListScheduleUseCaseReturn[]> {
    let schedules: ScheduleState[] = [];
    try {
      schedules = medicationName
        ? await this.scheduleDomain.findByMedicationName({ caregiverId, medicationName })
        : await this.scheduleDomain.findByCaregiver({ caregiverId });
    } catch (error) {
      throw new FetchingSchedulesError();
    }
    const schedulesWithDetailsPromises = schedules.map(async schedule => ({
      ...schedule,
      nextOccurrence: this.getNextOccurrence(schedule),
      slots: await this.getSlots(schedule)
    }));
    const schedulesWithDetails = await Promise.all(schedulesWithDetailsPromises);
    return this.sortSchedulesByNextOccurrence(schedulesWithDetails);

  }

  private sortSchedulesByNextOccurrence(schedules: ListScheduleUseCaseReturn[]): ListScheduleUseCaseReturn[] {
    return schedules.sort((a, b) => {
      const dateA = a.nextOccurrence ? a.nextOccurrence.getTime() : -Infinity;
      const dateB = b.nextOccurrence ? b.nextOccurrence.getTime() : -Infinity;
      return dateA - dateB; // Sorting ascending order
    });
  }

  private async getSlots(schedule: ScheduleState): Promise<number[]> {
    const slotsPlaced = await this.eventDomain.findPlaced({ scheduleIds: [schedule.scheduleId] });
    return slotsPlaced.map(event => event.idSlot);
  }

  private getNextOccurrence(schedule: ScheduleState): Date | null {
    const now = DateTime.now();
    const startDateTime = DateTime.fromJSDate(schedule.startDate).plus({ days: 1 });
    const endDateTime = DateTime.fromJSDate(schedule.endDate).plus({ days: 1 }).set({ hour: 23, minute: 59, second: 59, millisecond: 999 });
    if (now > endDateTime) {
      return null;
    }

    if (schedule.scheduleInterval) {
      return this.getNextOccurrenceForInterval(schedule, now, startDateTime, endDateTime);
    }

    if (schedule.times) {

      return this.getNextOccurrenceForSpecificTimes(schedule, now, startDateTime, endDateTime);
    }

    return null;
  }

  private getNextOccurrenceForInterval(
    schedule: ScheduleState,
    now: DateTime,
    startDateTime: DateTime,
    endDateTime: DateTime
  ): Date | null {
    let nextOccurrence = startDateTime.set({
      hour: parseInt(schedule.startTime.split(':')[0]),
      minute: parseInt(schedule.startTime.split(':')[1]),
      second: 0,
      millisecond: 0
    });

    const intervalMs = (schedule.intervalHours.hours * 60 + schedule.intervalHours.minutes) * 60000;

    while (nextOccurrence <= now) {
      nextOccurrence = nextOccurrence.plus({ milliseconds: intervalMs });
    }

    return nextOccurrence > endDateTime ? null : nextOccurrence.toJSDate();
  }

  private getNextOccurrenceForSpecificTimes(
    schedule: ScheduleState,
    now: DateTime,
    startDateTime: DateTime,
    endDateTime: DateTime
  ): Date | null {

    for (let date = startDateTime; date <= endDateTime; date = date.plus({ days: 1 })) {
      for (const time of schedule.times) {
        let nextOccurrence = date.set({
          hour: parseInt(time.split(':')[0]),
          minute: parseInt(time.split(':')[1]),
          second: 0,
          millisecond: 0
        });
        console.log(nextOccurrence.toJSDate())

        if (nextOccurrence > now) {
          console.log(nextOccurrence.toJSDate())
          return nextOccurrence.toJSDate();
        }
      }
    }

    return null;
  }
};
