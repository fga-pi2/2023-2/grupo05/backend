import ListScheduleUseCase from '..';
import ScheduleDomain from '../../../../domain/schedule';
import { ScheduleState } from '../../../../domain/schedule/types';
import { FetchingSchedulesError } from '../erros';
import ScheduleRepository from '../../../../domain/schedule/repository';
import EventRepository from '../../../../domain/event/repository';
import EventDomain from '../../../../domain/event';

const mockScheduleRepository: jest.Mocked<ScheduleRepository> = {
  create: jest.fn(),
  findByCaregiver: jest.fn(),
  findByMedicationName: jest.fn(),
  delete: jest.fn(),
  findOneBy: jest.fn(),
  update: jest.fn()


};

const mockEventRepository: jest.Mocked<EventRepository> = {
  create: jest.fn(),
  findPlaced: jest.fn(),
  findByRange: jest.fn(),
  update: jest.fn(),
  delete: jest.fn(),
  findNextEventAfterDate: jest.fn(),
  getNotTakenDispensed: jest.fn()

};




// Mock the ScheduleDomain class
const now = new Date();
const defaultMockedSchedule: ScheduleState = {
  scheduleInterval: false,
  scheduleId: 1, // Assign a mock ID
  startDate: new Date(now.getFullYear(), now.getMonth(), now.getDate()), // Mock start date as today
  endDate: new Date(now.getFullYear(), now.getMonth(), now.getDate() + 7), // Mock end date as one week from today
  startTime: '08:00', // Mock start time
  intervalHours: { hours: 6, minutes: 0 }, // Mock interval of every 6 hours
  times: ['08:00', '14:00', '20:00'], // Mock times array
  medicationName: 'Paracetamol', // Mock medication name
  medicationDescription: 'Used to treat pain and fever', // Mock medication description
  createdAt: now, // Use the current time for creation
  updatedAt: now, // Use the current time for last update
  caregiverId: 123, // Mock caregiver ID
}

// Setup a reusable factory function for mock ScheduleStates
const createMockSchedule = (overrides = {}): ScheduleState => {
  return { ...defaultMockedSchedule, ...overrides };
};

describe('ListScheduleUseCase', () => {
  let mockScheduleDomain: ScheduleDomain;
  let mockEventDomain: EventDomain;
  let useCase: ListScheduleUseCase;
  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:

    mockScheduleDomain = new ScheduleDomain(mockScheduleRepository)
    mockEventDomain = new EventDomain(mockEventRepository)
    useCase = new ListScheduleUseCase(mockScheduleDomain, mockEventDomain);
    useCase['getNextOccurrence'] = jest.fn().mockReturnValue(new Date("2023-12-02T05:00:00.000Z"));
  });

  it('should fetch schedules by caregiverId when no medicationName is provided', async () => {
    // Arrange
    const mockSchedules = [createMockSchedule()];
    mockScheduleDomain.findByCaregiver = jest.fn().mockResolvedValue(mockSchedules);
    mockEventDomain.findPlaced = jest.fn().mockResolvedValue([])

    // Act
    const result = await useCase.execute({ caregiverId: 1 });

    // Assert
    expect(mockScheduleDomain.findByCaregiver).toHaveBeenCalledWith({ caregiverId: 1 });
    const scheduleExpected = {
      ...mockSchedules[0],
      nextOccurrence: new Date("2023-12-02T05:00:00.000Z"),
      slots: []
    }
    expect(result).toEqual(expect.arrayContaining([scheduleExpected]));
  });

  it('should fetch schedules by medicationName when provided', async () => {
    // Arrange
    const mockSchedules = [createMockSchedule({ medicationName: 'Aspirin' })];
    mockScheduleDomain.findByMedicationName = jest.fn().mockResolvedValue(mockSchedules);
    mockEventDomain.findPlaced = jest.fn().mockResolvedValue([])

    // Act
    const result = await useCase.execute({ caregiverId: 1, medicationName: 'Aspirin' });

    // Assert
    expect(mockScheduleDomain.findByMedicationName).toHaveBeenCalledWith({ caregiverId: 1, medicationName: 'Aspirin' });
    const scheduleExpected = {
      ...mockSchedules[0],
      nextOccurrence: new Date("2023-12-02T05:00:00.000Z"),
      slots: []
    }
    expect(result).toEqual(expect.arrayContaining([scheduleExpected]));
  });

  it('should throw FetchingSchedulesError when an error occurs fetching schedules', async () => {
    // Arrange
    mockScheduleDomain.findByCaregiver = jest.fn().mockRejectedValue(new Error('Database error'));

    // Act & Assert
    await expect(useCase.execute({ caregiverId: 1 }))
      .rejects.toThrow(FetchingSchedulesError);
  });

  it('should return an empty array when `scheduleDomain` returns an empty array', async () => {
    // Arrange
    mockScheduleDomain.findByCaregiver = jest.fn().mockResolvedValue([]);

    // Act
    const result = await useCase.execute({ caregiverId: 1 });

    // Assert
    expect(result).toEqual([]);
  });

  it('should throw a custom error when `scheduleDomain` throws an unexpected error type', async () => {
    // Arrange
    const unexpectedError = new Error('Unexpected error');
    mockScheduleDomain.findByCaregiver = jest.fn().mockRejectedValue(unexpectedError);

    // Act & Assert
    await expect(useCase.execute({ caregiverId: 1 }))
      .rejects.toThrow(FetchingSchedulesError);
  });
});
