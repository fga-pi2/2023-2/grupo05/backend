import { ScheduleState } from "../../../domain/schedule/types";

export interface ListScheduleUseCaseReturn extends ScheduleState {
  nextOccurrence: Date | null;
  slots: number[];

}