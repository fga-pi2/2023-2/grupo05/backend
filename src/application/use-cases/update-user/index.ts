import * as bcrypt from "bcrypt";
import { UpdateUserUseCaseArgs } from "./input";
import { UserState } from "../../../domain/user/types";
import { InvalidPhoneNumberFormatError, UserNotFoundError } from "./erros";
import UserDomain from "../../../domain/user";

export default class UpdateUserUseCase {
  constructor(private userDomain: UserDomain) { }

  async execute({
    caregiverId,
    caregiverName,
    takerName,
    phoneNumber,
    email,
    password,
  }: UpdateUserUseCaseArgs) {

    let user: UserState = await this.userDomain.findOneBy({ caregiverId });

    if (!user) throw new UserNotFoundError();

    // Prepare the update data
    const updateData: Partial<UserState> = {};
    if (caregiverName) updateData.caregiverName = caregiverName;
    if (takerName) updateData.takerName = takerName;
    if (phoneNumber) {
      // Validate phone number format
      if (!this.validatePhoneNumber(phoneNumber)) {
        throw new InvalidPhoneNumberFormatError();
      }
      updateData.phoneNumber = phoneNumber;
    }
    if (email) updateData.email = email.toLowerCase();
    if (password) updateData.passwordHash = bcrypt.hashSync(password, 12);
    user = await this.userDomain.update(caregiverId, updateData);
    const { passwordHash, ...userData } = user;
    return { user: userData }
  }

  private validatePhoneNumber(phoneNumber: string): boolean {
    const regex = /^\(\d{2}\) \d{5}-\d{4}$/;
    return regex.test(phoneNumber);
  }
}
