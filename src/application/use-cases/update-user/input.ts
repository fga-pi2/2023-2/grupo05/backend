export type UpdateUserUseCaseArgs = {
  caregiverId?: number;
  caregiverName?: string;
  takerName?: string;
  email?: string;
  password?: string;
  phoneNumber?: string;
  createdAt?: Date;
  updatedAt?: Date;
};