
export class InvalidPhoneNumberFormatError extends Error {
  constructor() {
    super("Invalid phone number format. Expected format: (XX) XXXXX-XXXX, where X are digits.");
    this.name = "InvalidPhoneNumberFormatError";
  }
}

export class UserNotFoundError extends Error {
  constructor() {
    super("User not found");
  }
}