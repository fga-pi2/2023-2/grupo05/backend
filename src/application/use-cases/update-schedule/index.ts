
import { UpdateScheduleUseCaseArgs } from "./input";
import ScheduleDomain from "../../../domain/schedule";
import EventDomain from "../../../domain/event";
import { EndDateError, NegativeIntervalError, ZeroIntervalError, InvalidTimeFormatError, SlotsNotFreeError } from "../create-schedule/erros";
import { EventStatusSlot } from "../../../domain/event/types";
import { ScheduleState } from "../../../domain/schedule/types";
import { ScheduleNotFoundError } from "../delete-schedule/erros";
import { Interval } from "../../../domain/schedule/input";
import { DateTime } from 'luxon';

export default class UpdateScheduleUseCase {
  constructor(private scheduleDomain: ScheduleDomain, private eventDomain: EventDomain) { }

  async execute(args: UpdateScheduleUseCaseArgs): Promise<void> {
    const { scheduleInterval, slots, scheduleId, ...scheduleArgs } = args;

    const oldSchedule: ScheduleState = await this.scheduleDomain.findOneBy({ scheduleId });
    if (!oldSchedule) throw new ScheduleNotFoundError(scheduleId);
    this.validateArgs(args);

    await this.checkSlotsAvailability(args);

    this.prepareScheduleArguments(scheduleInterval, scheduleArgs);

    const newSchedule: Partial<ScheduleState> = this.prepareNewSchedule(scheduleArgs, args.startDate, args.endDate);
    const eventsToUpdate = await this.getEventsToUpdate(scheduleId, oldSchedule);
    await this.handleEventUpdates(scheduleInterval, args, eventsToUpdate, scheduleId);

    await this.scheduleDomain.update(scheduleId, newSchedule);
  }

  private async handleEventUpdates(
    scheduleInterval: boolean,
    args: UpdateScheduleUseCaseArgs,
    eventsToUpdate: any[],
    scheduleId: number
  ): Promise<void> {
    scheduleInterval
      ? await this.updateEventsWithInterval(args, eventsToUpdate, scheduleId)
      : await this.updateEventsWithSpecificTimes(args, eventsToUpdate, scheduleId);
  }

  private async getEventsToUpdate(scheduleId: number, oldSchedule: ScheduleState): Promise<any[]> {
    return await this.eventDomain.findByRange({
      scheduleIds: [scheduleId],
      startDate: oldSchedule.startDate,
      endDate: oldSchedule.endDate
    });
  }

  private prepareScheduleArguments(scheduleInterval: boolean, scheduleArgs: any): void {
    if (scheduleInterval) {
      scheduleArgs.times = null;
    } else {
      scheduleArgs.startTime = null;
      scheduleArgs.intervalHours = null;
    }
  }

  private prepareNewSchedule(scheduleArgs: any, startDate: string, endDate: string): Partial<ScheduleState> {
    const startDateTime = DateTime.fromISO(startDate);
    const endDateTime = DateTime.fromISO(endDate).set({ hour: 23, minute: 59, second: 59, millisecond: 999 });

    return {
      ...scheduleArgs,
      startDate: startDateTime.toJSDate(),
      endDate: endDateTime.toJSDate()
    };
  }

  private validateArgs(args: UpdateScheduleUseCaseArgs): void {
    this.validateDates(args.startDate, args.endDate);
    this.validateIntervalHours(args.intervalHours);
    if (args.startTime) this.validateTimeFormat(args.startTime);
    if (args.times) this.validateTimesArray(args.times);
  }

  private validateTimeFormat(startTime: string): void {
    if (!this.isValidTimeFormat(startTime)) {
      throw new InvalidTimeFormatError(startTime);
    }
  }

  private validateTimesArray(times: string[]): void {
    times.forEach(time => {
      if (!this.isValidTimeFormat(time)) {
        throw new InvalidTimeFormatError(time);
      }
    });
  }

  private validateDates(startDate: string, endDate: string): void {
    const startDateTime = DateTime.fromISO(startDate);
    const endDateTime = DateTime.fromISO(endDate).set({ hour: 23, minute: 59, second: 59, millisecond: 999 });

    if (endDateTime < startDateTime) {
      throw new EndDateError();
    }
  }

  private validateIntervalHours(intervalHours: any): void {
    if (intervalHours && (intervalHours.hours < 0 || intervalHours.minutes < 0)) {
      throw new NegativeIntervalError();
    }
    if (intervalHours && intervalHours.hours === 0 && intervalHours.minutes === 0) {
      throw new ZeroIntervalError();
    }
  }

  private async checkSlotsAvailability(args: UpdateScheduleUseCaseArgs): Promise<void> {
    const schedules = await this.scheduleDomain.findByCaregiver({ caregiverId: args.caregiverId });
    const scheduleIds = schedules.map(schedule => schedule.scheduleId);

    const slotsPlaced = await this.eventDomain.findPlaced({ scheduleIds });
    const placedSlotIds = slotsPlaced.filter(event => event.scheduleId != args.scheduleId).map(event => event.idSlot);
    const placedProvidedSlots = args.slots.filter(slot => placedSlotIds.includes(slot));

    if (placedProvidedSlots.length > 0) {
      throw new SlotsNotFreeError(placedProvidedSlots);
    }
  }

  private async updateEventsWithInterval(
    args: UpdateScheduleUseCaseArgs,
    eventsToUpdate: any[],
    scheduleId: number
  ): Promise<void> {
    let slotIndex = 0;
    let eventIndex = 0;
    const endDate = DateTime.fromISO(args.endDate).set({ hour: 23, minute: 59, second: 59, millisecond: 999 });;
    const startDate = DateTime.fromISO(args.startDate);
    const [startHours, startMinutes] = args.startTime.split(':').map(Number);
    let currentTime = startDate.set({ hour: startHours, minute: startMinutes, second: 0, millisecond: 0 });

    while (currentTime <= endDate) {

      if (currentTime > DateTime.now()) {
        eventIndex = await this.processEvent(eventIndex, currentTime, args.slots[slotIndex], eventsToUpdate, scheduleId);
        slotIndex++;
      }
      currentTime = this.incrementTime(currentTime, args.intervalHours);
    }

    await this.deleteRemainingEvents(eventIndex, eventsToUpdate);
  }

  private async updateEventsWithSpecificTimes(
    args: UpdateScheduleUseCaseArgs,
    eventsToUpdate: any[],
    scheduleId: number
  ): Promise<void> {
    args.times = this.sortTimes(args.times);
    let eventIndex = 0;
    let slotIndex = 0;
    const endDate = DateTime.fromISO(args.endDate).set({ hour: 23, minute: 59, second: 59, millisecond: 999 });
    const startDate = DateTime.fromISO(args.startDate);

    for (let date = startDate; date <= endDate; date = date.plus({ days: 1 })) {
      for (const time of args.times) {
        let takingDateTime = date.set({ hour: parseInt(time.split(':')[0]), minute: parseInt(time.split(':')[1]), second: 0, millisecond: 0 });
        if (takingDateTime > DateTime.now()) {
          await this.processEvent(eventIndex, takingDateTime, args.slots[slotIndex], eventsToUpdate, scheduleId);
          slotIndex++;
        }
        eventIndex++;
      }
    }
    await this.deleteRemainingEvents(eventIndex, eventsToUpdate);
  }

  private async processEvent(
    eventIndex: number,
    takingDateTime: DateTime,
    idSlot: number | null,
    eventsToUpdate: any[],
    scheduleId: number
  ): Promise<number> {
    if (eventsToUpdate[eventIndex]) {
      await this.updateEvent(eventsToUpdate[eventIndex].eventId, takingDateTime, idSlot);
    } else {
      await this.createEvent(scheduleId, takingDateTime, idSlot);
    }
    return eventIndex + 1;
  }



  private async deleteRemainingEvents(eventIndex: number, eventsToUpdate: any[]): Promise<void> {
    for (let i = eventIndex; i < eventsToUpdate.length; i++) {
      await this.eventDomain.delete(eventsToUpdate[i].eventId);
    }
  }

  private async updateEvent(eventId: number, takingDateTime: DateTime, idSlot: number | null): Promise<void> {
    const statusSlot = idSlot ? EventStatusSlot.PLACED : EventStatusSlot.EMPTY;

    await this.eventDomain.update(eventId, {
      takingDateTime: takingDateTime.toJSDate(),
      idSlot,
      statusSlot
    });
  }

  private async createEvent(scheduleId: number, takingDateTime: DateTime, idSlot: number | null): Promise<void> {
    const statusSlot = idSlot ? EventStatusSlot.PLACED : EventStatusSlot.EMPTY;
    await this.eventDomain.create({
      scheduleId,
      statusSlot,
      takingDateTime: takingDateTime.toJSDate(),
      idSlot
    });
  }

  private incrementTime(currentTime: DateTime, interval: Interval): DateTime {
    return currentTime.plus({ hours: interval.hours, minutes: interval.minutes });
  }

  private isValidTimeFormat(time: string): boolean {
    return /^([01]?\d|2[0-3]):[0-5]\d(:[0-5]\d)?$/.test(time);
  }

  private sortTimes(times: string[]): string[] {
    return times.sort((a, b) => {
      const [hoursA, minutesA] = a.split(':').map(Number);
      const [hoursB, minutesB] = b.split(':').map(Number);

      return hoursA * 60 + minutesA - (hoursB * 60 + minutesB);
    });
  }
}
