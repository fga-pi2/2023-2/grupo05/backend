

export type UpdateScheduleUseCaseArgs = {
  scheduleId: number;
  scheduleInterval: boolean;
  endDate: string | null;
  startDate: string | null;
  startTime: string | null;
  intervalHours: Interval | null;
  times: string[] | null;
  medicationName: string;
  medicationDescription: string | null;
  slots: number[];
  caregiverId: number;
}

type Interval = { hours: number, minutes: number }
