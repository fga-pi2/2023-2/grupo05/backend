export type UserState = {
  caregiverId: number;
  caregiverName: string;
  takerName: string;
  email: string;
  passwordHash: string;
  phoneNumber: string;
  createdAt: Date;
  updatedAt: Date;
};
