import { InvalidPhoneNumberFormatError, UserAlreadyExistsError, UserNotFoundError } from "./errors";
import { CreateArgs, UpdateArgs } from "./input";
import UserRepository from "./repository";
import { UserState } from "./types";
import * as bcrypt from "bcrypt";

export default class UserDomain {
  constructor(private repository: UserRepository) { }

  async create({
    caregiverName,
    takerName,
    phoneNumber,
    email,
    password,
  }: CreateArgs): Promise<UserState> {
    let databaseUser = await this.repository.findByEmail({ email });
    if (databaseUser) throw new UserAlreadyExistsError();

    const lowerCaseEmail = email.toLowerCase();
    const hashedPassword = bcrypt.hashSync(password, 12);

    if (!this.validatePhoneNumber(phoneNumber)) throw new InvalidPhoneNumberFormatError();

    return await this.repository.create({
      email: lowerCaseEmail,
      caregiverName,
      takerName,
      phoneNumber,
      passwordHash: hashedPassword,
    })


  }

  private validatePhoneNumber(phoneNumber: string): boolean {
    const regex = /^\(\d{2}\) \d{5}-\d{4}$/;
    return regex.test(phoneNumber);
  }

  async update(caregiverId: number, updateData: Partial<UpdateArgs>): Promise<UserState> {
    return await this.repository.update(caregiverId, updateData);
  }

  async findByEmail({ email }: { email: string }): Promise<UserState> {
    const user = await this.repository.findByEmail({ email });
    return user;

  }
  async findOneBy(criteria: Partial<Omit<UserState, "createdAt" | "updatedAt" | "passwordHash">>): Promise<UserState | null> {
    return await this.repository.findOneBy(criteria)
  }
}
