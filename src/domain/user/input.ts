export type CreateArgs = {
  caregiverName: string;
  takerName: string;
  phoneNumber: string;
  email: string;
  password: string;
};
export type UpdateArgs = {
  caregiverId?: number;
  caregiverName?: string;
  takerName?: string;
  email?: string;
  password?: string;
  passwordHash?: string;
  phoneNumber?: string;
  createdAt?: Date;
  updatedAt?: Date;
};