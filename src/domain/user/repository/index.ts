import { UpdateArgs } from "../input";
import { UserState } from "../types";
import { FindByEmailArgs } from "./input";

export default interface UserRepository {
  findByEmail(args: FindByEmailArgs): Promise<UserState | null>;
  create(user: Omit<UserState, "caregiverId" | "createdAt" | "updatedAt">): Promise<UserState | null>;
  findOneBy(criteria: Partial<Omit<UserState, "passwordHash" | "createdAt" | "updatedAt">>): Promise<UserState | null>;
  update(caregiverId: number, updateData: Partial<UpdateArgs>): Promise<UserState>;
  findByEmail({ email }: { email: string }): Promise<UserState>;
  findOneBy(criteria: Partial<Omit<UserState, "createdAt" | "updatedAt" | "passwordHash">>): Promise<UserState | null>;
}

