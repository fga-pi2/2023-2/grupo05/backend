export type FindByEmailArgs = {
  email: string;
};
