export class UserAlreadyExistsError extends Error {
  constructor() {
    super("User already exists");
  }
}

export class UserNotFoundError extends Error {
  constructor() {
    super("User not found");
  }
}

export class InvalidPasswordError extends Error {
  constructor() {
    super("Invalid password");
  }
}


export class InvalidPhoneNumberFormatError extends Error {
  constructor() {
    super("Invalid phone number format. Expected format: (XX) XXXXX-XXXX, where X are digits.");
    this.name = "InvalidPhoneNumberFormatError";
  }
}
