export type SendMessageArgs = {
  message: string;
  userPhoneNumber: string
};
