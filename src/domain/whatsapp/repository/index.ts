import { SendMessageArgs } from "./input";

export default interface WhatsappRepository {
  send(args: SendMessageArgs): Promise<void>
}
