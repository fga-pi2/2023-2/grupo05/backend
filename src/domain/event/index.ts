import EventRepository from "./repository";
import { CreateArgs, FindPlacedArgs, findByRangeArgs } from "./input";
import { EventState } from "./types";

export default class EventDomain {
  constructor(private eventRepository: EventRepository) { }

  async create(args: CreateArgs): Promise<void> {
    await this.eventRepository.create(args)
  }
  async findPlaced(args: FindPlacedArgs): Promise<EventState[]> {
    return await this.eventRepository.findPlaced(args)
  }
  async findByRange(args: findByRangeArgs): Promise<EventState[]> {
    return await this.eventRepository.findByRange(args)
  }
  async delete(eventId: number): Promise<void> {
    await this.eventRepository.delete(eventId);
  }
  async update(eventId: number, updateData: Partial<Omit<EventState, "eventId" | "createdAt" | "updatedAt">>): Promise<EventState | null> {
    return await this.eventRepository.update(eventId, updateData)
  }
  async findNextEventAfterDate(date: Date): Promise<EventState | null> {
    return await this.eventRepository.findNextEventAfterDate(date)
  }

  async getNotTakenDispensed(date: Date): Promise<EventState[]> {
    return await this.eventRepository.getNotTakenDispensed(date);
  }

}
