import { EventState } from "../types";
import { FindPlacedArgs, findByRangeArgs } from "../input";

export default interface EventRepository {
  create(args: Partial<Omit<EventState, "eventId" | "createdAt" | "updatedAt" | "medicationName" | "medicationDescription" | "isDiscarded" | "isDispensed">>): Promise<void>;
  findPlaced(args: FindPlacedArgs): Promise<EventState[] | null>;
  findByRange(args: findByRangeArgs): Promise<EventState[]>;
  delete(eventId: number): Promise<void>;
  update(eventId: number, updateData: Partial<Omit<EventState, "eventId" | "createdAt" | "updatedAt" | "medicationName" | "medicationDescription">>): Promise<EventState | null>;
  findNextEventAfterDate(date: Date): Promise<EventState | null>;
  getNotTakenDispensed(date: Date): Promise<EventState[]>;
}
