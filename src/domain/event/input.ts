
import { EventStatusSlot } from "./types";

export type CreateArgs = {
  scheduleId: number
  statusSlot: EventStatusSlot;
  takingDateTime: Date;
  idSlot: number | null;
}

export type FindPlacedArgs = {
  scheduleIds: number[];
}

export type findByRangeArgs = {
  scheduleIds: number[];
  startDate: Date;
  endDate: Date;
}