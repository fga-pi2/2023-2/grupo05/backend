export enum EventStatus {
  TAKEN = 'taken',
  NOT_TAKEN = 'noTaken',
}

export enum EventStatusSlot {
  EMPTY = 'empty',
  PLACED = 'placed',
}

export interface EventState {
  eventId: number;
  scheduleId: number | null;
  status: EventStatus;
  statusSlot: EventStatusSlot;
  medicationName: string | null;
  medicationDescription: string | null;
  caregiverId: number;
  takingDateTime: Date;
  idSlot: number | null;
  createdAt: Date;
  updatedAt: Date;
  isDispensed: boolean;
  isDiscarded: boolean;
}
