import { AlertState } from "../types";

export default interface AlertRepository {
  create(
    args: Omit<
      AlertState,
      | "alertId"
      | "caregiverId"
      | "eventId"
      | "message"
      | "createdAt"
      | "updatedAt"
      | "seen"
    >,
  ): Promise<void>;
  list(caregiverId: number): Promise<AlertState[]>;
  update(args: AlertState): Promise<AlertState>;
  findById(alertId: number): Promise<AlertState | undefined>;
}
