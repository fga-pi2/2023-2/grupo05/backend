export type CreateAlertArgs = {
  message: string;
  caregiverId: number;
  eventId?: number;
}
