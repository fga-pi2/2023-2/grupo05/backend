import UserRepository from '../user/repository';
import { UserState } from '../user/types';
import WhatsappRepository from '../whatsapp/repository';
import { CreateAlertArgs } from './input'
import AlertRepository from './repository'
import { AlertState } from './types'

export default class AlertDomain {
  constructor(private alertRepository: AlertRepository, private userRepository: UserRepository, private whatsappRepository: WhatsappRepository) { }

  async create({ caregiverId, message, eventId }: CreateAlertArgs): Promise<void> {
    await this.alertRepository.create({ caregiverId, message, eventId });
    const caregiver: UserState = await this.userRepository.findOneBy({ caregiverId })
    await this.whatsappRepository.send({
      userPhoneNumber: caregiver.phoneNumber,
      message
    })
  }

  async list(caregiverId: number): Promise<AlertState[]> {
    return await this.alertRepository.list(caregiverId)
  }

  async update(args: AlertState): Promise<AlertState> {
    return await this.alertRepository.update(args)
  }

  async findById(alertId: number): Promise<AlertState | undefined> {
    return await this.alertRepository.findById(alertId)
  }
}
