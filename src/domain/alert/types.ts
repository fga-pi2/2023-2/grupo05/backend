export interface AlertState {
  alertId: number;
  message?: string;
  caregiverId?: number;
  eventId?: number;
  createdAt?: Date;
  updatedAt?: Date;
  seen?: boolean;
}
