import { MedicineDispenseArgs } from "./inputs";
import { QueueMessage } from "./types";

export class EmbeddingDomain {

  private static msgQueue: QueueMessage[] = [];
  private static oldQueue: QueueMessage[] = [];

  static addMedicineToDispense(args: MedicineDispenseArgs): void {
    const queueMessage: QueueMessage = {
      endpoint: "/dispenser/dispense",
      body: {
        fileira: args.fileira,
        slot: args.slot,
        opened: false
      },
      method: "POST",
      timestamp: Date.now(),
    }

    this.msgQueue.push(queueMessage);
  }

  static openTrash(): void {
    const queueMessage: QueueMessage = {
      endpoint: "/dispenser/trash",
      body: {
        fileira: -1,
        slot: -1,
        opened: true
      },
      method: "POST",
      timestamp: Date.now(),
    }

    this.msgQueue.push(queueMessage);
  }

  static getAndClearMessages() {
    console.debug("queue", this.msgQueue)
    const currentQueue = this.msgQueue;
    this.oldQueue = this.oldQueue.concat(currentQueue);
    this.msgQueue = [];
    return currentQueue;
  }
  static getMsgQueue() {
    return this.msgQueue;
  }
  static getOldMsgQueue() {
    return this.oldQueue;
  }
}
