export type MedicineDispenseArgs = {
  fileira: number;
  slot: number;
};

export type QueueMessage = {
  endpoint: string;
  body: {
    fileira: number;
    slot: number;
  } | {};
  method: 'POST';
  timestamp: number;
};
