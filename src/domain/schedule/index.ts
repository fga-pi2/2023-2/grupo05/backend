import ScheduleRepository from "./repository";
import { CreateArgs, FindByCaregiverArgs, FindByMedicationNameArgs } from "./input";
import { ScheduleState } from "./types";

export default class ScheduleDomain {
  constructor(private scheduleRepository: ScheduleRepository) { }

  async create(args: CreateArgs): Promise<ScheduleState> {
    const schedule = await this.scheduleRepository.create(args)
    return schedule
  }

  async findByCaregiver({ caregiverId }: FindByCaregiverArgs): Promise<ScheduleState[]> {
    return await this.scheduleRepository.findByCaregiver({ caregiverId })
  }
  async findByMedicationName(args: FindByMedicationNameArgs): Promise<ScheduleState[]> {
    return await this.scheduleRepository.findByMedicationName(args)
  }

  async findOneBy(criteria: Partial<Omit<ScheduleState, "times">>): Promise<ScheduleState | null> {
    return await this.scheduleRepository.findOneBy(criteria)
  }

  async delete(scheduleId: number): Promise<void> {
    await this.scheduleRepository.delete(scheduleId);
  }

  async update(scheduleId: number, updateData: Partial<Omit<ScheduleState, "scheduleId" | "createdAt" | "updatedAt">>): Promise<ScheduleState | null> {
    return await this.scheduleRepository.update(scheduleId, updateData)
  }

}
