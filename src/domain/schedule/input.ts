export type CreateArgs = {
  endDate: Date | null;
  startDate: Date | null;
  startTime: string | null;
  scheduleInterval: boolean;
  intervalHours: Interval | null;
  times: string[] | null;
  medicationName: string;
  medicationDescription: string | null;
  caregiverId: number; // Uncomment and use if the CaregiverTaker entity is included
}

export type Interval = { hours: number, minutes: number }
export type FindByCaregiverArgs = {
  caregiverId: number;
}

export type FindByMedicationNameArgs = {
  caregiverId: number;
  medicationName: string;
}