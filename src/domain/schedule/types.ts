export interface ScheduleState {
  scheduleId: number;
  scheduleInterval: boolean;
  startDate: Date | null;
  endDate: Date | null;
  startTime: string | null;
  intervalHours: intervalHours | null;
  times: string[] | null;
  medicationName: string;
  medicationDescription: string | null;
  createdAt: Date;
  updatedAt: Date;
  caregiverId: number; // Uncomment and use if the CaregiverTaker entity is included
}

export type intervalHours = { hours: number, minutes: number }

