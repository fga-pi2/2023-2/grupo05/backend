import { ScheduleState } from "../types";
import { FindByCaregiverArgs, FindByMedicationNameArgs } from "../input";


export default interface ScheduleRepository {
  create(args: Omit<ScheduleState, "scheduleId" | "createdAt" | "updatedAt">): Promise<ScheduleState>;
  findByCaregiver(args: FindByCaregiverArgs): Promise<ScheduleState[]>;
  findByMedicationName(args: FindByMedicationNameArgs): Promise<ScheduleState[]>;
  delete(scheduleId: number): Promise<void>;
  findOneBy(criteria: Partial<Omit<ScheduleState, "times">>): Promise<ScheduleState | null>;
  update(scheduleId: number, updateData: Partial<Omit<ScheduleState, "scheduleId" | "createdAt" | "updatedAt">>): Promise<ScheduleState | null>;
}
