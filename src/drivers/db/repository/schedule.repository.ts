import { DataSource, Repository, ILike } from "typeorm";
import ScheduleRepository from "../../../domain/schedule/repository";
import { ScheduleState } from "../../../domain/schedule/types";
import { FindByCaregiverArgs, FindByMedicationNameArgs } from "../../../domain/schedule/input";
import { Schedule } from "../entity";

export class SchedulePostgresRepository implements ScheduleRepository {
  private scheduleRepository: Repository<Schedule>;

  constructor(private dataSource: DataSource) {
    this.scheduleRepository = this.dataSource.getRepository(Schedule);
  }

  async create(data: Omit<ScheduleState, "scheduleId">): Promise<ScheduleState> {
    const schedule = this.scheduleRepository.create(data);
    await this.scheduleRepository.save(schedule);
    return schedule
  }

  async findByCaregiver(args: FindByCaregiverArgs): Promise<ScheduleState[]> {
    const schedules = await this.scheduleRepository.find({
      where: {
        caregiverId: args.caregiverId
      }
    }
    )
    if (!schedules || schedules.length === 0) {
      return [];
    }
    return schedules.map(convertSQLScheduleToDomain)
  }

  async findByMedicationName(args: FindByMedicationNameArgs): Promise<ScheduleState[]> {
    const schedules = await this.scheduleRepository.find({
      where: {
        caregiverId: args.caregiverId,
        medicationName: ILike(`%${args.medicationName}%`)
      },
    });
    if (!schedules || schedules.length === 0) {
      return [];
    }
    return schedules.map(convertSQLScheduleToDomain)
  }

  async delete(scheduleId: number): Promise<void> {
    await this.scheduleRepository.delete(scheduleId)
  }
  async findOneBy(criteria: Partial<Omit<ScheduleState, "times">>): Promise<ScheduleState | null> {
    const schedule = await this.scheduleRepository.findOneBy(criteria);
    return schedule ? convertSQLScheduleToDomain(schedule) : null;
  }

  async update(scheduleId: number, updateData: Partial<Omit<ScheduleState, "scheduleId" | "createdAt" | "updatedAt">>): Promise<ScheduleState | null> {
    const schedule = await this.scheduleRepository.findOneBy({ scheduleId });
    if (!schedule) {
      return null;
    }
    await this.scheduleRepository.update(scheduleId, { ...schedule, ...updateData });
    return convertSQLScheduleToDomain({ ...schedule, ...updateData });
  }
}

export function convertSQLScheduleToDomain(schedule: Schedule): ScheduleState {
  return {
    scheduleId: schedule.scheduleId,
    scheduleInterval: schedule.scheduleInterval,
    medicationName: schedule.medicationName,
    medicationDescription: schedule.medicationDescription,
    startDate: new Date(schedule.startDate),
    endDate: new Date(schedule.endDate),
    startTime: schedule.startTime,
    intervalHours: schedule.intervalHours,
    times: schedule.times,
    caregiverId: schedule.caregiverId,
    createdAt: schedule.createdAt,
    updatedAt: schedule.updatedAt
  };
}

