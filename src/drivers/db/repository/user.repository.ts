import { DataSource, Repository } from "typeorm";
import UserRepository from "../../../domain/user/repository";
import { UserState } from "../../../domain/user/types";
import { User } from "../entity";

export class UserPostgresRepository implements UserRepository {
  private userRepository: Repository<User>;

  constructor(private dataSource: DataSource) {
    this.userRepository = this.dataSource.getRepository(User);
  }

  async findByEmail({ email }: { email: string }): Promise<UserState> {
    const user = await this.userRepository.findOne({ where: { email } });

    if (!user) return null;

    return convertSQLUserToDomain(user);
  }
  async findOneBy(criteria: Partial<Omit<UserState, "passwordHash" | "createdAt" | "updatedAt">>): Promise<UserState | null> {
    const user = await this.userRepository.findOneBy(criteria);
    return user ? convertSQLUserToDomain(user) : null;
  }
  async create(user: Omit<UserState, "caregiverId" | "createdAt" | "updatedAt">): Promise<UserState> {
    const newUser = this.userRepository.create(user);
    await this.userRepository.save(newUser);

    return convertSQLUserToDomain(newUser);
  }
  async update(caregiverId: number, updateData: Partial<Omit<UserState, "createdAt" | "updatedAt">>): Promise<UserState | null> {
    const user = await this.userRepository.findOneBy({ caregiverId: caregiverId });

    if (!user) return null;

    Object.assign(user, updateData);

    await this.userRepository.save(user);

    return convertSQLUserToDomain(user);
  }


}

export function convertSQLUserToDomain(user: User): UserState {
  return {
    caregiverId: user.caregiverId,
    caregiverName: user.caregiverName,
    takerName: user.takerName,
    phoneNumber: user.phoneNumber,
    email: user.email,
    passwordHash: user.passwordHash,
    createdAt: user.createdAt,
    updatedAt: user.updatedAt
  };
}
