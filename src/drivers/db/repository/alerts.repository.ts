import { DataSource, Repository } from 'typeorm';
import { Alert } from '../entity';
import { AlertState } from '../../../domain/alert/types';
import AlertRepository from '../../../domain/alert/repository';


export class AlertPostgresRepository implements AlertRepository {
  private readonly alertRepository: Repository<Alert>

  constructor(dataSource: DataSource) {
    this.alertRepository = dataSource.getRepository(Alert)
  }

  async create(
    event: Omit<
      AlertState,
      | "alertId"
      | "caregiverId"

      | "idEvent"
      | "createdAt"
      | "updatedAt"
      | "seen"
    >,
  ): Promise<void> {
    const alertsEntity = this.alertRepository.create(event)
    await this.alertRepository.save(alertsEntity)
  }

  async list(caregiverId: number): Promise<AlertState[]> {
    const alerts = await this.alertRepository.find(
      {
        where: {
          caregiverId: caregiverId
        },
        order: {
          createdAt: 'ASC'
        },
      }
    )
    if (!alerts || alerts.length === 0) {
      return []
    }
    return alerts.map(convertSQLAlertsToDomain)
  }

  async update(args: AlertState): Promise<AlertState> {
    const alert = await this.alertRepository.findOne(
      {
        where: {
          alertId: args.alertId
        }
      }
    );
    if (!alert) {
      return;
    }
    alert.message = args.message;
    alert.seen = args.seen;
    alert.caregiverId = args.caregiverId;

    await this.alertRepository.save(alert);
    return convertSQLAlertsToDomain(alert);
  }

  async findById(alertId: number): Promise<AlertState | undefined> {
    const alert = await this.alertRepository.findOne(
      {
        where: {
          alertId: alertId
        }
      })
    if (!alert) {
      return undefined
    }
    return convertSQLAlertsToDomain(alert)
  }
}

function convertSQLAlertsToDomain(alert: Alert): AlertState | null {
  if (!alert) {
    return null;
  }
  return {
    alertId: alert.alertId,
    message: alert.message,
    caregiverId: alert.caregiverId,
    eventId: alert.eventId,
    createdAt: alert.createdAt,
    updatedAt: alert.updatedAt,
    seen: alert.seen
  }
}
