import { DataSource, Repository, In, Between, MoreThanOrEqual, LessThanOrEqual } from "typeorm";
import EventRepository from "../../../domain/event/repository";
import { EventState, EventStatus, EventStatusSlot } from "../../../domain/event/types";

import { Event } from "../entity";
import { FindPlacedArgs, findByRangeArgs } from "../../../domain/event/input";

export class EventPostgresRepository implements EventRepository {
  private eventRepository: Repository<Event>;

  constructor(dataSource: DataSource) {
    this.eventRepository = dataSource.getRepository(Event);
  }

  async create(event: Partial<Omit<EventState, "eventId" | "createdAt" | "updatedAt" | "medicationName" | "medicationDescription" | "isDiscarded" | "isDispensed">>): Promise<void> {
    await this.eventRepository.save(event);
  }

  async findPlaced(args: FindPlacedArgs): Promise<EventState[]> {
    const events = await this.eventRepository.find({
      where: {
        scheduleId: In(args.scheduleIds),
        statusSlot: EventStatusSlot.PLACED
      },
      relations: ['schedule']
    });
    if (!events || events.length === 0) {
      return [];
    }
    return events.map(convertSQLEventToDomain);
  }
  async findByRange(args: findByRangeArgs): Promise<EventState[]> {
    const events = await this.eventRepository.find({
      where: {
        scheduleId: In(args.scheduleIds),
        takingDateTime: Between(args.startDate, args.endDate)
      },
      relations: ['schedule']
    });
    if (!events || events.length === 0) {
      return [];
    }
    return events.map(convertSQLEventToDomain);
  }
  async update(eventId: number, updateData: Partial<Omit<EventState, "eventId" | "createdAt" | "updatedAt" | "medicationName" | "medicationDescription">>): Promise<EventState | null> {
    const event = await this.eventRepository.findOneBy({ eventId });
    if (!event) {
      return null;
    }
    await this.eventRepository.update(eventId, { ...event, ...updateData });
    return convertSQLEventToDomain({ ...event, ...updateData });
  }
  async delete(eventId: number): Promise<void> {
    await this.eventRepository.delete(eventId)
  }
  async findNextEventAfterDate(date: Date): Promise<EventState | null> {
    const event = await this.eventRepository.findOne({
      where: {
        takingDateTime: MoreThanOrEqual(date)
      },
      order: {
        takingDateTime: 'ASC'
      },
      relations: ['schedule']
    });

    return event ? convertSQLEventToDomain(event) : null;
  }

  async getNotTakenDispensed(date: Date): Promise<EventState[]> {
    const events = await this.eventRepository.find({
      where: {
        takingDateTime: LessThanOrEqual(date),
        isDispensed: true,
        status: EventStatus.NOT_TAKEN
      },
      order: {
        takingDateTime: 'DESC'
      },
      relations: ['schedule']
    });

    return events.map(convertSQLEventToDomain);
  }
}



export function convertSQLEventToDomain(event: Event): EventState | null {
  if (!event) {
    return null;
  }
  return {
    eventId: event.eventId,
    caregiverId: event.schedule ? event.schedule.caregiverId : null,
    scheduleId: event.scheduleId,
    takingDateTime: event.takingDateTime,
    status: event.status,
    statusSlot: event.statusSlot,
    idSlot: event.idSlot,
    isDispensed: event.isDispensed,
    createdAt: event.createdAt,
    updatedAt: event.updatedAt,
    isDiscarded: event.isDiscarded,
    medicationName: event.schedule ? event.schedule.medicationName : null,
    medicationDescription: event.schedule ? event.schedule.medicationDescription : null,
  };
}