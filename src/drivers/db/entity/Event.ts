import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn, UpdateDateColumn, JoinColumn, OneToMany } from "typeorm";
import { EventStatus, EventStatusSlot } from "../../../domain/event/types";
import { Schedule } from "./Schedule";
import { Alert } from "./Alerts";


@Entity()
export class Event {
  @PrimaryGeneratedColumn()
  eventId: number;

  @Column({ nullable: true })
  scheduleId: number;

  @ManyToOne(() => Schedule, schedule => schedule.events, { onDelete: "SET NULL" })
  @JoinColumn({ name: 'scheduleId' })
  schedule: Schedule;

  @Column({ type: "timestamp" })
  takingDateTime: Date;

  @Column({
    type: 'enum',
    enum: EventStatus,
    default: EventStatus.NOT_TAKEN
  })
  status: EventStatus;

  @Column({
    type: 'enum',
    enum: EventStatusSlot,
    default: EventStatusSlot.EMPTY
  })
  statusSlot: EventStatusSlot;

  @Column({ nullable: true })
  idSlot: number | null;

  @Column({ default: false })
  isDispensed: boolean;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @OneToMany(() => Alert, alert => alert.event)
  alerts: Alert[];

  @Column({ default: false })
  isDiscarded: boolean;

}
