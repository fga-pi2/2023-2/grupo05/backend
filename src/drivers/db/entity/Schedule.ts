import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn } from "typeorm";
import { Event } from "./Event";
import { intervalHours } from "../../../domain/schedule/types";
import { User } from "./User";

@Entity()
export class Schedule {
  @PrimaryGeneratedColumn()
  scheduleId: number;

  @Column({ type: 'date' })
  startDate: Date;

  @Column({ type: 'date' })
  endDate: Date;

  @Column()
  scheduleInterval: boolean;

  @Column({ type: 'json', nullable: true })
  intervalHours: intervalHours | null;

  @Column({ type: "time", nullable: true })
  startTime: string; // Stored in 'HH:MM:SS' format

  @Column({ nullable: true })
  medicationDescription: string | null;

  @Column()
  medicationName: string;

  @Column({ type: 'json', nullable: true })
  times: string[] | null;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @Column({ nullable: true })
  caregiverId: number;

  @OneToMany(() => Event, event => event.schedule)
  events: Event[];

  @ManyToOne(() => User, user => user.schedules, { onDelete: "SET NULL" })
  @JoinColumn({ name: 'caregiverId' })
  user: User;
}
