import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { Schedule } from "./Schedule";
import { Alert } from "./Alerts";

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  caregiverId: number;

  @Column()
  caregiverName: string;

  @Column()
  takerName: string;

  @Column()
  phoneNumber: string;

  @Column()
  email: string;

  @Column()
  passwordHash: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @OneToMany(() => User, schedule => schedule.caregiverId)
  schedules: Schedule[];

  @OneToMany(() => Alert, alert => alert.caregiverId)
  alerts: Alert[];
}
