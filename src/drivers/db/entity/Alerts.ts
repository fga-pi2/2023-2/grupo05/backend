import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn } from 'typeorm'
import { User } from './User'
import { Event } from './Event'

@Entity()
export class Alert {
  @PrimaryGeneratedColumn()
  alertId: number

  @Column()
  message: string

  @Column({})
  caregiverId: number

  @Column({ nullable: true })
  eventId: number

  @Column({ default: false })
  seen: boolean

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne(() => User, user => user.alerts, { onDelete: "CASCADE" })
  @JoinColumn({ name: 'caregiverId' })
  user: User;

  @ManyToOne(() => Event, event => event.alerts, { onDelete: "SET NULL" })
  @JoinColumn({ name: 'eventId' })
  event: Event;
}
