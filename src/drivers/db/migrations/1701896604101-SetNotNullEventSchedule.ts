import { MigrationInterface, QueryRunner } from "typeorm";

export class SetNotNullEventSchedule1701896604101 implements MigrationInterface {
    name = 'SetNotNullEventSchedule1701896604101'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" DROP CONSTRAINT "FK_78f4792105a2ed6013b3c063f8e"`);
        await queryRunner.query(`ALTER TABLE "event" ALTER COLUMN "scheduleId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "event" ADD CONSTRAINT "FK_78f4792105a2ed6013b3c063f8e" FOREIGN KEY ("scheduleId") REFERENCES "schedule"("scheduleId") ON DELETE SET NULL ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" DROP CONSTRAINT "FK_78f4792105a2ed6013b3c063f8e"`);
        await queryRunner.query(`ALTER TABLE "event" ALTER COLUMN "scheduleId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "event" ADD CONSTRAINT "FK_78f4792105a2ed6013b3c063f8e" FOREIGN KEY ("scheduleId") REFERENCES "schedule"("scheduleId") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
