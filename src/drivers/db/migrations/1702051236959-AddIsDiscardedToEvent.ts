import { MigrationInterface, QueryRunner } from "typeorm";

export class AddIsDiscardedToEvent1702051236959 implements MigrationInterface {
    name = 'AddIsDiscardedToEvent1702051236959'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" ADD "isDiscarded" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "isDiscarded"`);
    }

}
