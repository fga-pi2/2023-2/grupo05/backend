import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateAlertTable1702007159935 implements MigrationInterface {
    name = 'CreateAlertTable1702007159935'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "alert" ("alertId" SERIAL NOT NULL, "message" character varying NOT NULL, "caregiverId" integer NOT NULL, "eventId" integer, "seen" boolean NOT NULL DEFAULT false, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_9ccb2852f976044582773bcd313" PRIMARY KEY ("alertId"))`);
        await queryRunner.query(`ALTER TABLE "alert" ADD CONSTRAINT "FK_236ad9a38496fa19c8ee5a81117" FOREIGN KEY ("caregiverId") REFERENCES "user"("caregiverId") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "alert" ADD CONSTRAINT "FK_459ca132e88818419bc25509fd8" FOREIGN KEY ("eventId") REFERENCES "event"("eventId") ON DELETE SET NULL ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "alert" DROP CONSTRAINT "FK_459ca132e88818419bc25509fd8"`);
        await queryRunner.query(`ALTER TABLE "alert" DROP CONSTRAINT "FK_236ad9a38496fa19c8ee5a81117"`);
        await queryRunner.query(`DROP TABLE "alert"`);
    }

}
