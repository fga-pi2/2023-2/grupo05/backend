import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateScheduleAndEventTables1701633414482 implements MigrationInterface {
    name = 'CreateScheduleAndEventTables1701633414482'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "schedule" ("scheduleId" SERIAL NOT NULL, "startDate" date NOT NULL, "endDate" date NOT NULL, "scheduleInterval" boolean NOT NULL, "intervalHours" json, "startTime" TIME, "medicationDescription" character varying, "medicationName" character varying NOT NULL, "times" json, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "caregiverId" integer NOT NULL, CONSTRAINT "PK_c7e04f67bbcc2bd68ec5c996e1f" PRIMARY KEY ("scheduleId"))`);
        await queryRunner.query(`CREATE TYPE "public"."event_status_enum" AS ENUM('taken', 'noTaken')`);
        await queryRunner.query(`CREATE TYPE "public"."event_statusslot_enum" AS ENUM('empty', 'placed')`);
        await queryRunner.query(`CREATE TABLE "event" ("eventId" SERIAL NOT NULL, "scheduleId" integer NOT NULL, "takingDateTime" TIMESTAMP NOT NULL, "status" "public"."event_status_enum" NOT NULL DEFAULT 'noTaken', "statusSlot" "public"."event_statusslot_enum" NOT NULL DEFAULT 'empty', "idSlot" integer, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_4ee8fd974a5681971c4eb5bb585" PRIMARY KEY ("eventId"))`);
        await queryRunner.query(`ALTER TABLE "event" ADD CONSTRAINT "FK_78f4792105a2ed6013b3c063f8e" FOREIGN KEY ("scheduleId") REFERENCES "schedule"("scheduleId") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" DROP CONSTRAINT "FK_78f4792105a2ed6013b3c063f8e"`);
        await queryRunner.query(`DROP TABLE "event"`);
        await queryRunner.query(`DROP TYPE "public"."event_statusslot_enum"`);
        await queryRunner.query(`DROP TYPE "public"."event_status_enum"`);
        await queryRunner.query(`DROP TABLE "schedule"`);
    }

}
