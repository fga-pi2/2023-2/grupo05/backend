import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateUSerTable1702004525325 implements MigrationInterface {
    name = 'CreateUSerTable1702004525325'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "user" ("caregiverId" SERIAL NOT NULL, "caregiverName" character varying NOT NULL, "takerName" character varying NOT NULL, "phoneNumber" character varying NOT NULL, "email" character varying NOT NULL, "passwordHash" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_889e2d6dfbd22ec185a18bd04db" PRIMARY KEY ("caregiverId"))`);
        await queryRunner.query(`ALTER TABLE "schedule" ALTER COLUMN "caregiverId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "schedule" ADD CONSTRAINT "FK_d8c5f3400a61a7d81ed9298505f" FOREIGN KEY ("caregiverId") REFERENCES "user"("caregiverId") ON DELETE SET NULL ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "schedule" DROP CONSTRAINT "FK_d8c5f3400a61a7d81ed9298505f"`);
        await queryRunner.query(`ALTER TABLE "schedule" ALTER COLUMN "caregiverId" SET NOT NULL`);
        await queryRunner.query(`DROP TABLE "user"`);
    }

}
