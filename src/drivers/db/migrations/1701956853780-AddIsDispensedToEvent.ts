import { MigrationInterface, QueryRunner } from "typeorm";

export class AddIsDispensedToEvent1701956853780 implements MigrationInterface {
    name = 'AddIsDispensedToEvent1701956853780'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" ADD "isDispensed" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "isDispensed"`);
    }

}
