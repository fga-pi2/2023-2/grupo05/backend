import "reflect-metadata";
import { DataSource } from "typeorm";

const is_dev = process.env["DEV"] == "true";
console.log("DEV =", is_dev);

export const dataSource = new DataSource({
  type: "postgres",
  host: is_dev ? "localhost" : "db",
  port: 5432,
  username: "postgres",
  password: "123456",
  database: "postgres",
  schema: "public",
  synchronize: false,
  logging: false,
  entities: [__dirname + "/entity/*.*"],
  migrations: [__dirname + "/migrations/*.*"],
});
