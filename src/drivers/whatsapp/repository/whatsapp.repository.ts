import { Twilio } from 'twilio'
import WhatsappRepository from '../../../domain/whatsapp/repository';
import { SendMessageArgs } from '../../../domain/whatsapp/repository/input';

export class WhatsappTwilioRepository implements WhatsappRepository {
  private twilioClient: Twilio;
  private defaultFrom: string = 'whatsapp:+14155238886';  // Default 'from' number

  constructor() {
    this.twilioClient = new Twilio(
      'AC7b64dabffcb8aad8aa81eff8bbe8a576',
      '0db76c52cc4642a482ffec82c5dfc5ec'
    )
  }

  async send({ message, userPhoneNumber }: SendMessageArgs): Promise<void> {
    const formattedPhoneNumber = this.formatPhoneNumber(userPhoneNumber);
    await this.twilioClient.messages.create({
      body: message,
      from: this.defaultFrom,
      to: formattedPhoneNumber
    }).then(() => {
      console.log(this.twilioClient.httpClient.lastResponse.statusCode);
      console.log(this.twilioClient.httpClient.lastResponse.body);
    }).catch((error) => {
      console.log(error);
    });
    console.log('WhatsApp message sent to:', userPhoneNumber, 'original number', 'with message:', message)
  }
  private formatPhoneNumber(phoneNumber: string): string {
    // Extract the area code (DDD) and the phone number without the first 9
    const areaCode = phoneNumber.slice(1, 3);
    const numberWithoutFirst9 = phoneNumber.slice(6, 10) + phoneNumber.slice(11);

    // Combine with the country code (+55)
    return `whatsapp:+55${areaCode}${numberWithoutFirst9}`;
  }
}
