import cron from 'node-cron';
import DispenseMedicineUseCase from '../../application/use-cases/dispense-medicine';
import DispenseTrashUseCase from '../../application/use-cases/dispense-trash';
import EventDomain from '../../domain/event';
import AlertDomain from '../../domain/alert';
import { dataSource } from '../db/data-source';
import { EventPostgresRepository } from '../db/repository/event.repository';
import { AlertPostgresRepository } from '../db/repository/alerts.repository';
import { UserPostgresRepository } from '../db/repository/user.repository';
import { WhatsappTwilioRepository } from '../whatsapp/repository/whatsapp.repository';

const eventRepository = new EventPostgresRepository(dataSource);
const eventDomain = new EventDomain(eventRepository);
const alertRepository = new AlertPostgresRepository(dataSource);
const userRepository = new UserPostgresRepository(dataSource);
const whatsappRepository = new WhatsappTwilioRepository();
const alertDomain = new AlertDomain(alertRepository, userRepository, whatsappRepository);

const dispenseMedicineUseCase = new DispenseMedicineUseCase(eventDomain, alertDomain);
const dispenseTrashUseCase = new DispenseTrashUseCase(eventDomain, alertDomain);


function executeJob(useCase, jobName, actionDescription) {
  console.debug(`${jobName}: Started at ${new Date().toISOString()}. Action: ${actionDescription}`);

  try {
    useCase.execute()
      .then(() => console.debug(`${jobName}: Completed action at ${new Date().toISOString()}`))
      .catch((error) => console.error(`${jobName}: Error occurred - ${error.message}`, error));
  } catch (error) {
    console.error(`${jobName}: Unexpected error occurred - ${error.message}`, error);
  }
}

export const dispenseMedicineJob = cron.schedule('1 * * * * *', () => {
  executeJob(dispenseMedicineUseCase, 'dispenseMedicineJob', 'Checking for medicine to dispense');
}, {
  scheduled: false
});

export const openTrashJob = cron.schedule('*/30 * * * * *', () => {
  executeJob(dispenseTrashUseCase, 'openTrashJob', 'Checking for medicine to discard');
}, {
  scheduled: false
});

