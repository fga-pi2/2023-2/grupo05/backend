import { Router } from "express";
import { adaptRoute } from "../adapters/route-adapter";
import { RegisterControllerFactory } from "../factories/user/register";
import { loginControllerFactory } from "../factories/user/login";
import { UpdateUserControllerFactory } from "../factories/user/update";

const authRouter = Router();

authRouter.post(
  "/login",
  adaptRoute(loginControllerFactory())
);

authRouter.post("/register", adaptRoute(RegisterControllerFactory()));
authRouter.put("/user/update/:caregiverId", adaptRoute(UpdateUserControllerFactory()));


export default authRouter;
