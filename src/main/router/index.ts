import { Router } from "express";
import authRouter from "./auth";
import scheduleRouter from "./schedule";
import eventRouter from "./event";
import longpollRouter from "./longpoll";
import alertRouter from "./alerts";
import dispenserRouter from "./dispenser";

const router = Router();

router.use(authRouter);
router.use(scheduleRouter);
router.use(eventRouter);
router.use(longpollRouter);
router.use(alertRouter);
router.use(dispenserRouter);

export default router;
