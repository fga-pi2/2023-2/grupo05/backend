import { Router } from 'express'
import { adaptRoute } from '../adapters/route-adapter'
import { ListAlertsControllerFactory } from '../factories/alerts/list-alert.factories'
import { UpdateAlertControllerFactory } from '../factories/alerts/update-alert.factories'

const alertRouter = Router()

alertRouter.get('/alerts/list', adaptRoute(ListAlertsControllerFactory()));
alertRouter.put('/alerts/:alertId', adaptRoute(UpdateAlertControllerFactory()));
export default alertRouter