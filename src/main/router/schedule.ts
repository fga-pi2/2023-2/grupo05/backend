import { Router } from "express";
import { adaptRoute } from "../adapters/route-adapter";
import { CreateScheduleControllerFactory } from "../factories/schedule/create-schedule.factories";
import { ListScheduleControllerFactory } from "../factories/schedule/list-schedule.factories";
import { DeleteScheduleControllerFactory } from "../factories/schedule/delete-schedule.factories";
import { UpdateScheduleControllerFactory } from "../factories/schedule/update-schedule.factories";
const scheduleRouter = Router();

scheduleRouter.post("/schedule/new", adaptRoute(CreateScheduleControllerFactory()));
scheduleRouter.get("/schedule/list/:caregiverId", adaptRoute(ListScheduleControllerFactory()));
scheduleRouter.delete("/schedule/:scheduleId", adaptRoute(DeleteScheduleControllerFactory()));
scheduleRouter.put("/schedule/:scheduleId", adaptRoute(UpdateScheduleControllerFactory()));

export default scheduleRouter;
