import { Router } from 'express'
import { adaptRoute } from '../adapters/route-adapter'
import { ListSlotsFilledControllerFactory } from '../factories/dispenser/list-slot-filled.factories';
import { DispenserDrawerControllerFactory } from '../factories/dispenser/drawer-factories';

const dispenserRouter = Router()

dispenserRouter.get('/slot', adaptRoute(ListSlotsFilledControllerFactory()));
dispenserRouter.get("/dispenser/drawer", adaptRoute(DispenserDrawerControllerFactory()));
export default dispenserRouter