import { Router } from "express";
import { adaptRoute } from "../adapters/route-adapter";
import { CreateLongPollFactory, CreateLongPollTesterFactory } from "../factories/longpoll/longpoll.factories";
const longpoll = Router();

longpoll.post("/longpoll", adaptRoute(CreateLongPollFactory()));
longpoll.get("/longpoll", adaptRoute(CreateLongPollFactory()));
longpoll.post("/longpoll-test", adaptRoute(CreateLongPollTesterFactory()));

export default longpoll;
