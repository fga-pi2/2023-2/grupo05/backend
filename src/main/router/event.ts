import { Router } from "express";
import { adaptRoute } from "../adapters/route-adapter";
import { ListEventControllerFactory } from "../factories/event/list-event.factories";
const eventRouter = Router();

eventRouter.get("/event/list/:caregiverId", adaptRoute(ListEventControllerFactory()));

export default eventRouter;
