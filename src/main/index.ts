import express, { Application, Request, Response } from "express";
import { dataSource } from "../drivers/db/data-source";
import router from "./router";
import cors from "cors";
import { dispenseMedicineJob, openTrashJob } from "../drivers/cron";
import { Settings } from 'luxon';

// Set default time zone to 'America/Sao_Paulo' (Brazil time zone)
Settings.defaultZone = 'America/Sao_Paulo';

// Set default locale to 'pt-BR' (Brazilian Portuguese)
Settings.defaultLocale = 'pt-BR';

dataSource
  .initialize()
  .then(() => {
    console.log("Data Source has been initialized!");
  })
  .catch((err) => {
    console.error("Error during Data Source initialization:", err);
  });

const app = express();
app.use(cors());
app.use(express.json());
app.use(router);

app.post("/ping", (req, res) => {
  console.log(req);
  res.send("pong");
});


app.get("/health", (_req: Request, res: Response, _next) => {
  const healthCheck = {
    uptime: process.uptime(),
    message: "OK",
    timestamp: Date.now(),
    test: 123,
  };

  try {
    res.send(healthCheck);
  } catch (error) {
    healthCheck.message = error;
    res.status(503).send(healthCheck);
  }
});

app.get("/", (_req: Request, res: Response, _next) => {
  res.json({
    message: "Sim! O backend tá rodando!",
    endpoints: getRoutes(app)
  });
});

console.log("App rodando em http://localhost:3000")

dispenseMedicineJob.start()
openTrashJob.start()
app.listen(3000);












function getRoutesOfLayer(path: string, layer: any): string[] {
  if (layer.method) {
    return [layer.method.toUpperCase() + ' ' + path];
  }
  else if (layer.route) {
    return getRoutesOfLayer(path + split(layer.route.path), layer.route.stack[0]);
  }
  else if (layer.name === 'router' && layer.handle.stack) {
    let routes: string[] = [];

    layer.handle.stack.forEach(function (stackItem: any) {
      routes = routes.concat(getRoutesOfLayer(path + split(layer.regexp), stackItem));
    });

    return routes;
  }

  return [];
}
function split(thing: any): string {
  if (typeof thing === 'string') {
    return thing;
  } else if (thing.fast_slash) {
    return '';
  } else {
    var match = thing.toString()
      .replace('\\/?', '')
      .replace('(?=\\/|$)', '$')
      .match(/^\/\^((?:\\[.*+?^${}()|[\]\\\/]|[^.*+?^${}()|[\]\\\/])*)\$\//)
    return match
      ? match[1].replace(/\\(.)/g, '$1')
      : '<complex:' + thing.toString() + '>';
  }
}
function getRoutes(app: Application): string[] {
  let routes: string[] = [];

  app._router.stack.forEach(function (layer: any) {
    routes = routes.concat(getRoutesOfLayer('', layer));
  });

  return routes;
}