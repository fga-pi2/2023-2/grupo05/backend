import ListSlotsFilledUseCase from "../../../application/use-cases/list-slots-filled";
import { dataSource } from "../../../drivers/db/data-source";
import SlotsFilledDomain from "../../../domain/event";
import ScheduleDomain from "../../../domain/schedule";
import { SchedulePostgresRepository } from "../../../drivers/db/repository/schedule.repository";
import { EventPostgresRepository } from "../../../drivers/db/repository/event.repository";
import { ListSlotsFilledController } from "../../../adapters/controllers/dispenser/list-slots.controllers";



export function ListSlotsFilledControllerFactory() {
  const eventRepository = new EventPostgresRepository(dataSource);
  const scheduleRepository = new SchedulePostgresRepository(dataSource);

  const eventDomain = new SlotsFilledDomain(eventRepository);
  const scheduleDomain = new ScheduleDomain(scheduleRepository);

  const listSlotsFilledUseCase = new ListSlotsFilledUseCase(eventDomain, scheduleDomain);

  const controller = new ListSlotsFilledController(listSlotsFilledUseCase);

  return controller;
}