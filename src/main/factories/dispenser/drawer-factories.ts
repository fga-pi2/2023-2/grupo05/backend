import { DispenseDrawerController } from "../../../adapters/controllers/dispenser/drawer.controller";
import DispenseDrawerUseCase from "../../../application/use-cases/dispense-drawer";
import AlertDomain from "../../../domain/alert";
import EventDomain from "../../../domain/event";
import { AlertPostgresRepository } from "../../../drivers/db/repository/alerts.repository";
import { EventPostgresRepository } from "../../../drivers/db/repository/event.repository";
import { UserPostgresRepository } from "../../../drivers/db/repository/user.repository";
import { dataSource } from "../../../drivers/db/data-source";
import { WhatsappTwilioRepository } from "../../../drivers/whatsapp/repository/whatsapp.repository";





export function DispenserDrawerControllerFactory() {
  const eventRepository = new EventPostgresRepository(dataSource);
  const alertRepository = new AlertPostgresRepository(dataSource);
  const userRepository = new UserPostgresRepository(dataSource);
  const whatsappRepository = new WhatsappTwilioRepository();

  const eventDomain = new EventDomain(eventRepository);
  const alertDomain = new AlertDomain(alertRepository, userRepository, whatsappRepository);

  const dispenseDrawerUseCase = new DispenseDrawerUseCase(eventDomain, alertDomain);

  const controller = new DispenseDrawerController(dispenseDrawerUseCase);

  return controller;
}