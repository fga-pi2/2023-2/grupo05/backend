import { DeleteScheduleController } from "../../../adapters/controllers/schedule/delete-schedule.controllers";
import DeleteScheduleUseCase from "../../../application/use-cases/delete-schedule";
import { dataSource } from "../../../drivers/db/data-source";
import { SchedulePostgresRepository } from "../../../drivers/db/repository/schedule.repository";
import ScheduleDomain from "../../../domain/schedule";
import EventDomain from "../../../domain/event";
import { EventPostgresRepository } from "../../../drivers/db/repository/event.repository";


export function DeleteScheduleControllerFactory() {
  const scheduleRepository = new SchedulePostgresRepository(dataSource);
  const eventRepository = new EventPostgresRepository(dataSource);

  const scheduleDomain = new ScheduleDomain(scheduleRepository);
  const eventDomain = new EventDomain(eventRepository);

  const deleteScheduleUseCase = new DeleteScheduleUseCase(scheduleDomain, eventDomain);
  const controller = new DeleteScheduleController(deleteScheduleUseCase);

  return controller;
}