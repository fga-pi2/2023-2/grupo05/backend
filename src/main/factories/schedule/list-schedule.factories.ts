import { ListScheduleController } from "../../../adapters/controllers/schedule/list-schedule.controllers";
import ListScheduleUseCase from "../../../application/use-cases/list-schedule";
import { dataSource } from "../../../drivers/db/data-source";
import { SchedulePostgresRepository } from "../../../drivers/db/repository/schedule.repository";
import ScheduleDomain from "../../../domain/schedule";
import { EventPostgresRepository } from "../../../drivers/db/repository/event.repository";
import EventDomain from "../../../domain/event";


export function ListScheduleControllerFactory() {
  const scheduleRepository = new SchedulePostgresRepository(dataSource);
  const eventRepository = new EventPostgresRepository(dataSource);

  const scheduleDomain = new ScheduleDomain(scheduleRepository);
  const eventDomain = new EventDomain(eventRepository);

  const listScheduleUseCase = new ListScheduleUseCase(scheduleDomain, eventDomain);
  const controller = new ListScheduleController(listScheduleUseCase);

  return controller;
}