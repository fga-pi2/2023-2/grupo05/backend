import { CreateScheduleController } from "../../../adapters/controllers/schedule/create-schedule.controllers";
import CreateScheduleUseCase from "../../../application/use-cases/create-schedule";
import { dataSource } from "../../../drivers/db/data-source";
import { SchedulePostgresRepository } from "../../../drivers/db/repository/schedule.repository";
import { EventPostgresRepository } from "../../../drivers/db/repository/event.repository";
import ScheduleDomain from "../../../domain/schedule";
import EventDomain from "../../../domain/event";


export function CreateScheduleControllerFactory() {
  const scheduleRepository = new SchedulePostgresRepository(dataSource);
  const eventRepository = new EventPostgresRepository(dataSource);

  const scheduleDomain = new ScheduleDomain(scheduleRepository);
  const eventDomain = new EventDomain(eventRepository);

  const createScheduleUseCase = new CreateScheduleUseCase(scheduleDomain, eventDomain);
  const controller = new CreateScheduleController(createScheduleUseCase);

  return controller;
}