import { UpdateScheduleController } from "../../../adapters/controllers/schedule/update-schedule.controllers";
import UpdateScheduleUseCase from "../../../application/use-cases/update-schedule";
import { dataSource } from "../../../drivers/db/data-source";
import { SchedulePostgresRepository } from "../../../drivers/db/repository/schedule.repository";
import ScheduleDomain from "../../../domain/schedule";
import { EventPostgresRepository } from "../../../drivers/db/repository/event.repository";
import EventDomain from "../../../domain/event";


export function UpdateScheduleControllerFactory() {
  const scheduleRepository = new SchedulePostgresRepository(dataSource);
  const eventRepository = new EventPostgresRepository(dataSource);

  const scheduleDomain = new ScheduleDomain(scheduleRepository);
  const eventDomain = new EventDomain(eventRepository);

  const listScheduleUseCase = new UpdateScheduleUseCase(scheduleDomain, eventDomain);
  const controller = new UpdateScheduleController(listScheduleUseCase);

  return controller;
}