import { UpdateAlertController } from '../../../adapters/controllers/alert/update-alert.controllers'
import { dataSource } from '../../../drivers/db/data-source'
import { AlertPostgresRepository } from '../../../drivers/db/repository/alerts.repository'

import AlertDomain from '../../../domain/alert'
import UpdateAlertUseCase from '../../../application/use-cases/update-alert'
import { UserPostgresRepository } from '../../../drivers/db/repository/user.repository'
import { WhatsappTwilioRepository } from '../../../drivers/whatsapp/repository/whatsapp.repository'

export const UpdateAlertControllerFactory = (): UpdateAlertController => {
    const alertRepository = new AlertPostgresRepository(dataSource);
    const userRepository = new UserPostgresRepository(dataSource);

    const whatsappRepository = new WhatsappTwilioRepository();

    const alertDomain = new AlertDomain(alertRepository, userRepository, whatsappRepository);
    const updateAlertUseCase = new UpdateAlertUseCase(alertDomain)
    const updateAlertController = new UpdateAlertController(updateAlertUseCase)
    return updateAlertController
}