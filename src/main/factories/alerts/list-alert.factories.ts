import { ListAlertController } from '../../../adapters/controllers/alert/list-alert.controllers'
import { dataSource } from '../../../drivers/db/data-source'
import { AlertPostgresRepository } from '../../../drivers/db/repository/alerts.repository'

import AlertDomain from '../../../domain/alert'
import ListAlertsUseCase from '../../../application/use-cases/list-alert'
import { WhatsappTwilioRepository } from '../../../drivers/whatsapp/repository/whatsapp.repository'
import { UserPostgresRepository } from '../../../drivers/db/repository/user.repository'

export function ListAlertsControllerFactory() {
  const alertRepository = new AlertPostgresRepository(dataSource);
  const userRepository = new UserPostgresRepository(dataSource);

  const whatsappRepository = new WhatsappTwilioRepository();

  const alertDomain = new AlertDomain(alertRepository, userRepository, whatsappRepository);

  const createAlertUseCase = new ListAlertsUseCase(alertDomain);

  const controller = new ListAlertController(createAlertUseCase);

  return controller
}
