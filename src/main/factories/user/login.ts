import { LoginController } from "../../../adapters/controllers";
import LoginUseCase from "../../../application/use-cases/login";
import UserDomain from "../../../domain/user";
import { dataSource } from "../../../drivers/db/data-source";
import { UserPostgresRepository } from "../../../drivers/db/repository/user.repository";

export function loginControllerFactory() {
  const userRepository = new UserPostgresRepository(dataSource);
  const userDomain = new UserDomain(userRepository);
  const loginUseCase = new LoginUseCase(userDomain);
  const controller = new LoginController(loginUseCase);

  return controller;
}
