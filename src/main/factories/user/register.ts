import { RegisterController } from "../../../adapters/controllers/user/register.controller";
import RegisterUseCase from "../../../application/use-cases/register";
import UserDomain from "../../../domain/user";
import { dataSource } from "../../../drivers/db/data-source";
import { UserPostgresRepository } from "../../../drivers/db/repository/user.repository";

export function RegisterControllerFactory() {
  const userRepository = new UserPostgresRepository(dataSource);
  const userDomain = new UserDomain(userRepository);
  const registerUseCase = new RegisterUseCase(userDomain);
  const controller = new RegisterController(registerUseCase);

  return controller;
}
