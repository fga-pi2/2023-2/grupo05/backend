import { RegisterController } from "../../../adapters/controllers/user/register.controller";
import { UpdateController } from "../../../adapters/controllers/user/update.controllers";
import RegisterUseCase from "../../../application/use-cases/register";
import UpdateUserUseCase from "../../../application/use-cases/update-user";
import UserDomain from "../../../domain/user";
import { dataSource } from "../../../drivers/db/data-source";
import { UserPostgresRepository } from "../../../drivers/db/repository/user.repository";

export function UpdateUserControllerFactory() {
  const userRepository = new UserPostgresRepository(dataSource);
  const userDomain = new UserDomain(userRepository);
  const updateUserUseCase = new UpdateUserUseCase(userDomain);
  const controller = new UpdateController(updateUserUseCase);

  return controller;
}
