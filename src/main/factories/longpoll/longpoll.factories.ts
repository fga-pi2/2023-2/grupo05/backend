import { LongPollController, LongPollTesterController } from "../../../adapters/controllers/longpoll/longpoll.controllers";

export function CreateLongPollFactory() {

  const controller = new LongPollController();

  return controller;
}

export function CreateLongPollTesterFactory() {
  const controller = new LongPollTesterController();

  return controller;
}