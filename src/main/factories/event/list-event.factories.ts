import { ListEventController } from "../../../adapters/controllers/event/list-event.controllers";
import ListEventUseCase from "../../../application/use-cases/list-event";
import { dataSource } from "../../../drivers/db/data-source";
import { EventPostgresRepository } from "../../../drivers/db/repository/event.repository";
import EventDomain from "../../../domain/event";
import ScheduleDomain from "../../../domain/schedule";
import { SchedulePostgresRepository } from "../../../drivers/db/repository/schedule.repository";



export function ListEventControllerFactory() {
  const eventRepository = new EventPostgresRepository(dataSource);
  const scheduleRepository = new SchedulePostgresRepository(dataSource);

  const eventDomain = new EventDomain(eventRepository);
  const scheduleDomain = new ScheduleDomain(scheduleRepository);

  const listEventUseCase = new ListEventUseCase(eventDomain, scheduleDomain);

  const controller = new ListEventController(listEventUseCase);

  return controller;
}