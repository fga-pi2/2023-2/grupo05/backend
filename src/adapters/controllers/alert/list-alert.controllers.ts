import { Controller } from "../../protocols/controller";
import { StatusCodes } from 'http-status-codes';
import ListAlertUseCase from "../../../application/use-cases/list-alert";

export class ListAlertController implements Controller {
  constructor(private ListAlertUseCase: ListAlertUseCase) { }


  async handle(request: any): Promise<{ statusCode: number, body: any }> {
    const { caregiverId } = request;
    try {
      const results = await this.ListAlertUseCase.execute(caregiverId)

      return {
        statusCode: StatusCodes.OK,
        body: { results }
      };
    } catch (error) {
      const errorResponse = {
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
        message: `Internal Server Error: ${error.message}`
      };
      return {
        statusCode: errorResponse.statusCode,
        body: { message: errorResponse.message }
      };
    }
  }
}
