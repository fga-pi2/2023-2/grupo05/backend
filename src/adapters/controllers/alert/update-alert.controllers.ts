import { Controller } from "../../protocols/controller";
import UpdateAlertUseCase from "../../../application/use-cases/update-alert";
import { StatusCodes } from 'http-status-codes';

export class UpdateAlertController implements Controller {
constructor(
    private readonly updateAlertUseCase: UpdateAlertUseCase
) { }

async handle(request: any): Promise<any> {
    try {
        const { alertId,
            message,
            seen,
            caregiverId,
            eventId } = request
        const return_value = await this.updateAlertUseCase.execute({
            alertId,
            message,
            seen,
            caregiverId,
            eventId
        });
        return {
            statusCode: StatusCodes.OK,
            body: return_value
        }
    } catch (error) {
        return {
            statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
            body: { "message": "Erro ao tentar realizar a atualização do alerta" }
        }
    }
}
    
}