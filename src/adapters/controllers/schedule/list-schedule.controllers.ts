import { Controller } from "../../protocols/controller";
import { StatusCodes } from 'http-status-codes';
import ListScheduleUseCase from "../../../application/use-cases/list-schedule";

export class ListScheduleController implements Controller {
  constructor(private listScheduleUseCase: ListScheduleUseCase) { }


  async handle(request: any): Promise<{ statusCode: number, body: any }> {
    const { caregiverId, medicationName } = request;
    try {
      const results = await this.listScheduleUseCase.execute({ caregiverId, medicationName })

      return {
        statusCode: StatusCodes.OK,
        body: { results }
      };
    } catch (error) {
      console.error('Error while list  schedules:', error);
      const errorResponse = {
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
        message: `Internal Server Error: ${error.message}`
      };
      return {
        statusCode: errorResponse.statusCode,
        body: { message: errorResponse.message }
      };
    }
  }
}
