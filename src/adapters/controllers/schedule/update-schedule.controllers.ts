import { Controller } from "../../protocols/controller";
import { StatusCodes } from 'http-status-codes';
import { EndDateError, ZeroIntervalError, NegativeIntervalError, InvalidTimeFormatError, SlotsNotFreeError } from "../../../application/use-cases/create-schedule/erros";
import UpdateScheduleUseCase from "../../../application/use-cases/update-schedule";
import { ScheduleNotFoundError } from "../../../application/use-cases/delete-schedule/erros";

export class UpdateScheduleController implements Controller {
  constructor(private updateScheduleUseCase: UpdateScheduleUseCase) { }

  private knownErrors = [
    SlotsNotFreeError,
    EndDateError,
    ZeroIntervalError,
    NegativeIntervalError,
    InvalidTimeFormatError,
    ScheduleNotFoundError
  ];


  async handle(request: any): Promise<{ statusCode: number, body: any }> {
    const {
      scheduleId,
      scheduleInterval,
      endDate,
      startDate,
      startTime,
      intervalHours,
      times,
      medicationName,
      medicationDescription,
      slots,
      caregiverId
    } = request;
    try {
      console.debug({
        scheduleId,
        scheduleInterval,
        endDate,
        startDate,
        startTime,
        intervalHours,
        times,
        medicationName,
        medicationDescription,
        slots,
        caregiverId
      })
      await this.updateScheduleUseCase.execute({
        scheduleId,
        scheduleInterval,
        endDate,
        startDate,
        startTime,
        intervalHours,
        times,
        medicationName,
        medicationDescription,
        slots,
        caregiverId
      });

      return {
        statusCode: StatusCodes.OK,
        body: { message: 'Schedule edited successfully' }

      };
    } catch (error) {
      console.error('Error while editing a schedule:', error);

      const isErrorKnown = this.knownErrors.some(knownError => error instanceof knownError);

      if (isErrorKnown) {
        return {
          statusCode: StatusCodes.BAD_REQUEST,
          body: { message: error.message }
        };
      }

      return {
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
        body: { message: `Internal Server Error: ${error.message}` }
      };
    }
  }
}
