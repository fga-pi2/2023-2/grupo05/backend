import { Controller } from "../../protocols/controller";
import { StatusCodes } from 'http-status-codes';
import DeleteScheduleUseCase from "../../../application/use-cases/delete-schedule";
import { ScheduleNotFoundError, InvalidScheduleError } from "../../../application/use-cases/delete-schedule/erros";

export class DeleteScheduleController implements Controller {
  constructor(private deleteScheduleUseCase: DeleteScheduleUseCase) { }
  private knownErrors = new Map<Function, number>([
    [ScheduleNotFoundError, StatusCodes.NOT_FOUND],
    [InvalidScheduleError, StatusCodes.BAD_REQUEST]
  ]);
  async handle(request: any): Promise<{ statusCode: number, body: any }> {
    const { scheduleId } = request;
    try {
      await this.deleteScheduleUseCase.execute(scheduleId);

      return {
        statusCode: StatusCodes.OK,
        body: { message: 'Schedule deleted successfully' }
      };
    } catch (error) {
      console.error('Error while deleting schedules:', error);
      const errorStatusCode = this.knownErrors.get(error.constructor);
      const statusCode = errorStatusCode || StatusCodes.INTERNAL_SERVER_ERROR;
      const message = errorStatusCode ? error.message : `Internal Server Error: ${error.message}`;
      return {
        statusCode,
        body: { message }
      };
    }
  }
}
