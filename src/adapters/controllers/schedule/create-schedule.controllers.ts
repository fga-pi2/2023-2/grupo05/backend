import { Controller } from "../../protocols/controller";
import { StatusCodes } from 'http-status-codes';
import { EndDateError, ZeroIntervalError, NegativeIntervalError, InvalidTimeFormatError, SlotsNotFreeError } from "../../../application/use-cases/create-schedule/erros";
import CreateScheduleUseCase from "../../../application/use-cases/create-schedule";

export class CreateScheduleController implements Controller {
  constructor(private createScheduleUseCase: CreateScheduleUseCase) { }

  private knownErrors = [
    SlotsNotFreeError,
    EndDateError,
    ZeroIntervalError,
    NegativeIntervalError,
    InvalidTimeFormatError,
  ];


  async handle(request: any): Promise<{ statusCode: number, body: any }> {
    const {
      scheduleInterval,
      endDate,
      startDate,
      startTime,
      intervalHours,
      times,
      medicationName,
      medicationDescription,
      slots,
      caregiverId
    } = request;
    try {
      await this.createScheduleUseCase.execute({
        scheduleInterval,
        endDate,
        startDate,
        startTime,
        intervalHours,
        times,
        medicationName,
        medicationDescription,
        slots,
        caregiverId
      });

      return {
        statusCode: StatusCodes.CREATED,
        body: { message: 'Schedule created successfully' }
      };
    } catch (error) {
      console.error('Error while creating a schedule:', error);

      const isErrorKnown = this.knownErrors.some(knownError => error instanceof knownError);

      if (isErrorKnown) {
        return {
          statusCode: StatusCodes.BAD_REQUEST,
          body: { message: error.message }
        };
      }

      return {
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
        body: { message: `Internal Server Error: ${error.message}` }
      };
    }
  }
}
