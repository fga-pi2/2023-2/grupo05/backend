import { StatusCodes } from "http-status-codes";
import UpdateUserUseCase from "../../../application/use-cases/update-user";
import { Controller } from "../../protocols/controller";

export class UpdateController implements Controller {
  constructor(private updateUseCase: UpdateUserUseCase) { }

  async handle(request: any): Promise<any> {
    const {
      caregiverId,
      caregiverName,
      takerName,
      phoneNumber,
      email,
      password,
    } = request;

    try {
      const response = await this.updateUseCase.execute({
        caregiverId,
        caregiverName,
        takerName,
        phoneNumber,
        email,
        password,
      });

      return {
        statusCode: StatusCodes.OK,
        body: { message: 'User updated successfully', ...response }
      };
    } catch (error) {
      console.error('Error while updating a user:', error);
      return {
        statusCode: StatusCodes.BAD_REQUEST,
        body: { message: error.message }
      };
    }
  }
}
