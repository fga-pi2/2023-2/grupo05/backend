import { StatusCodes } from "http-status-codes";
import RegisterUseCase from "../../../application/use-cases/register";
import { Controller } from "../../protocols/controller";

export class RegisterController implements Controller {
  constructor(private RegisterUseCase: RegisterUseCase) { }

  async handle(request: any): Promise<any> {
    const {
      caregiverName,
      takerName,
      phoneNumber,
      email,
      password,
    } = request;
    try {
      const response = await this.RegisterUseCase.execute({
        caregiverName,
        takerName,
        phoneNumber,
        email,
        password,
      });
      return {
        statusCode: StatusCodes.CREATED,
        body: { message: 'User created successfully', ...response }

      };
    } catch (error) {
      console.error('Error while creating a user:', error);
      return {
        statusCode: StatusCodes.BAD_REQUEST,
        body: { message: error.message }
      };
    }
  }
}
