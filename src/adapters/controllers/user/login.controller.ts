import { StatusCodes } from "http-status-codes";
import LoginUseCase from "../../../application/use-cases/login";
import { Controller } from "../../protocols/controller";

export class LoginController implements Controller {
  constructor(private loginUseCase: LoginUseCase) { }

  async handle(request: any): Promise<any> {
    const { email, password } = request;
    try {

      const response = await this.loginUseCase.execute({
        email,
        password,
      });
      return {
        statusCode: StatusCodes.ACCEPTED,
        body: { message: 'User authenticated successfully', ...response }

      };
    } catch (error) {
      console.error('Error when try login:', error);
      return {
        statusCode: StatusCodes.BAD_REQUEST,
        body: { message: error.message }
      };
    }
  }
}
