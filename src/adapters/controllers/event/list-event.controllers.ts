import { Controller } from "../../protocols/controller";
import { StatusCodes } from 'http-status-codes';
import ListEventUseCase from "../../../application/use-cases/list-event";

export class ListEventController implements Controller {
  constructor(private listEventUseCase: ListEventUseCase) { }


  async handle(request: any): Promise<{ statusCode: number, body: any }> {
    const { caregiverId, targetDate } = request;
    try {
      const results = await this.listEventUseCase.execute({ caregiverId, targetDate })

      return {
        statusCode: StatusCodes.OK,
        body: { results }
      };
    } catch (error) {
      console.error('Error while list  events:', error);
      const errorResponse = {
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
        message: `Internal Server Error: ${error.message}`
      };
      return {
        statusCode: errorResponse.statusCode,
        body: { message: errorResponse.message }
      };
    }
  }
}
