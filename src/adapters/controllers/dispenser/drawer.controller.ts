import DispenseDrawerUseCase from "../../../application/use-cases/dispense-drawer";
import { Controller } from "../../protocols/controller";
import { StatusCodes } from 'http-status-codes';

export class DispenseDrawerController implements Controller {
  constructor(private dispenseDrawerUseCase: DispenseDrawerUseCase) { }


  async handle(request: any): Promise<{ statusCode: number, body: any }> {

    const { open } = request;
    try {
      const openStatus: boolean = typeof open === 'string' && open.toLowerCase().includes('false');

      if (openStatus) {
        return {
          statusCode: StatusCodes.OK,
          body: { message: "Drawer opening not registered!" }
        };
      }
      const results = await this.dispenseDrawerUseCase.execute({ open: true })

      return {
        statusCode: StatusCodes.ACCEPTED,
        body: {
          message: "Drawer opening registered successfully!",
          results
        }
      };
    } catch (error) {
      console.error('Open Drawer:', error);
      const errorResponse = {
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
        message: `Internal Server Error: ${error.message}`
      };
      return {
        statusCode: errorResponse.statusCode,
        body: { message: errorResponse.message }
      };
    }
  }
}
