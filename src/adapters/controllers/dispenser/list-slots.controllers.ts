import ListSlotsFilledUseCase from "../../../application/use-cases/list-slots-filled";
import { Controller } from "../../protocols/controller";
import { StatusCodes } from 'http-status-codes';

export class ListSlotsFilledController implements Controller {
  constructor(private listSlotsFilledUseCase: ListSlotsFilledUseCase) { }


  async handle(request: any): Promise<{ statusCode: number, body: any }> {

    const { caregiverId } = request;
    try {
      const results = await this.listSlotsFilledUseCase.execute({ caregiverId })

      return {
        statusCode: StatusCodes.OK,
        body: { results }
      };
    } catch (error) {
      console.error('Error while list  slots:', error);
      const errorResponse = {
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
        message: `Internal Server Error: ${error.message}`
      };
      return {
        statusCode: errorResponse.statusCode,
        body: { message: errorResponse.message }
      };
    }
  }
}
