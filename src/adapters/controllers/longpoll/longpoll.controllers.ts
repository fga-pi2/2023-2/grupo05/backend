import { EmbeddingDomain } from "../../../domain/embedding";
import { Controller } from "../../protocols/controller";
import { StatusCodes } from "http-status-codes";

export class LongPollController implements Controller {

  async handle(request: any): Promise<{ statusCode: number; body: any }> {
    try {
      const messages = EmbeddingDomain.getAndClearMessages();
      console.debug(messages)
      return {
        statusCode: StatusCodes.OK,
        body: messages,
      };
    } catch (error) {
      return {
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
        body: { message: `Internal Server Error: ${error.message}` },
      };
    }
  }
}



export class LongPollTesterController implements Controller {
  constructor() { }

  private knownErrors = [];

  async handle(request: any): Promise<{ statusCode: number; body: any }> {
    const data = request;
    try {
      console.log("LongPollTestController.handle", data);

      const fileira = data.fileira;
      const slot = data.slot;

      EmbeddingDomain.addMedicineToDispense({ slot, fileira });

      return {
        statusCode: StatusCodes.OK,
        body: {
          message: "Adicionei chamada na fila!!",
          fila: EmbeddingDomain.getMsgQueue(),
          oldFile: EmbeddingDomain.getOldMsgQueue(),
        },
      };
    } catch (error) {
      console.error("Error in /longpoll:", error);

      return {
        statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
        body: { message: `Internal Server Error: ${error.message}` }
      };
    }
  }
}